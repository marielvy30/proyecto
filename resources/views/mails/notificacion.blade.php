<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
</head>
<body>


<h3>Hola {{ $name }}, por el presente medio se le notifica que han violentado un/varios artículo(s) de la ley 63-17 (Leyes de Tránsito) <strong></strong></h3>

<h3>Agente SIMV: </h3> {{ AUTH::user()->name }}<strong></strong>

<h3>Lugar del hecho: </h3> {{ $direccion }} <strong></strong>

<h3>Hora del hecho: </h3> {{ $hora }} <strong></strong>

<h3>Día del hecho: </h3> {{ $fecha }} <strong></strong>

<h3>Ubicación: Latitud/longitud</h3>{{$latitud}} , {{$longitud}}<strong></strong>




<h3>Para mayor información hacer clic en el link</h3>
<a href="http://18.190.117.21/login">
    Ver más información
</a>


<p> Se levanta la presente acta de infracción que hace constar la violación al(los) artículo(s) de la ley 63-17
    marcado(s) en la(s) casilla(s) de más arriba. El ciudadano cuenta con un plazo de treinta (30) días a partir de la
    fecha de emisión de esta acta que vale como citación legal, para a) pagar voluntariamente la multa en los puntos
    establecidos para estos fines, cuyo importe será el menor del rango contemplado en la Ley para la(s) sanción(es)
    correspondiente(s): o b) ejercer su derecho de impugnar en justicia en el Juzgado de Paz Ordinario de la demarcación
    done ocurrió el hecho o ante la Unidad de Transito del Ministerio Publico. De no pagar voluntariamente o no impugnar
    la infracción en el plazo establecido el infractor reconoce que no aplicaría el importe menor de rango contemplado en
    la Ley y que los pagos realizados después de vencido el citado plazo incluirán los recargos contemplados en el Código
    Tributario y en sus leyes complementarias, entre otras sanciones contempladas en la Ley, sin necesidad de nueva notificación. </p>

<p> Si usted no ha cometido o no esta de acuerdo con dicha infracción puede apelar la infracción a través de nuestro
    Sistema de servicio. Usted cuenta con un plazo de treinta (30) días a partir de la fecha de emisión de esta acta
    que vale como citación legal.
</p>

</body>
</html>
