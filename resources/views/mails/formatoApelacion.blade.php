@include('layouts.messages')
    <!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>Apelacion</title>
    <style>
        body {
            font-family: sans-serif, verdana, arial;
        }
        table tr td:first-child
        {
            text-align: right;
        }
    </style>
</head>
<body>

<form>
    <table>
        <tr>
            <td  style="text-align: center" >Al: </td>
        </tr>
        <tr>
            <td style="text-align: center"><p>Tribunal de Tránsito de Santiago </p></td>
        </tr>
        <tr>
            <td  style="text-align: center" >Asunto: </td>
        </tr>
        <tr>
            <td  style="text-align: center"> <p>Solicitud de Levantamiento de multa</p></td>
        </tr>
        <tr>
            <td  style="text-align: center">Nombre del Solicitante: </td>
        </tr>
        <tr>
            <td style="text-align: center" value=""><p>{{Auth::user()->name}}</p></td>
        </tr>

        <tr>
            <td style="text-align: left">HONORABLE MAGISTRADO/A:</td>
            <td></td>
        </tr>
        <tr>
            <td style="text-align: justify">
            <p>
                Quien suscribe, el señor/a {{Auth::user()->name}} Dominicano/a, mayor de edad, titular de la cédula de identidad y electoral No. {{Auth::user()->cedula}}, domiciliado/a y residente en esta ciudad de Santiago, República Dominicana, teléfono No.{{Auth::user()->telefono}} tiene a bien exponerle y solicitarle lo siguiente:
            </p>
            </td>
            <td></td>
        </tr>
        <tr>
            <td style="text-align: justify">

                <p>
                    EXPONE: Que se ha recibido noticación de denuncia formulada por supuesta infracción al (los) artículo(s)
                    @foreach($facturas as $factura) 
                    @foreach ($factura->multaarticulo as $articulo)
                        {{$factura->articulo_num = ""}}
                        {{ $factura->articulo_num .= ($articulo->numero_articulo . "")}}
                        {{","}}
                    @endforeach
                    @endforeach de la Ley 63-17.
                    Que no estando conforme en absoluto con los hechos denunciados, interpone el presente escrito en base a las siguientes,

                </p>
            </td>

        </tr>

        <tr>
            <td style="text-align: justify">
                <p>{{$alegacion1}}</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify">
                <p>{{$alegacion2}}</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify">
                <p>{{$alegacion3}}</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify">
                <p>{{$alegacion4}}</p>
            </td>
        </tr>

        <tr>
            <td style="text-align: justify">
                <p>{{$solicitud1}}</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify">
                <p>{{$solicitud2}}
                </p>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify">
                <p>{{$solicitud3}}</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify">
                <p>{{$solicitud4}}</p>
            </td>
        </tr>
    <?php
        setlocale(LC_TIME, 'spanish');
        $mes =("m");
        $inicio = strftime("%B", strtotime($mes));
    ?>
        <tr>
            <td style="text-align: justify">
                <p>
            En la ciudad de Santiago de los Caballeros, a los <?php echo date("d"); ?> días del mes de {{$inicio}} del año dos mil veinte (<?php echo date("Y"); ?>).
                </p>
            </td>
            <td></td>
        </tr>
        <tr>
            <center>
                <pre><img src="./firma/{{Auth::user()->foto_firma}}" style="max-width: 200px; max-height: 250px"></pre>
                Firma del Solicitante
            </center>
        </tr>
    </table>
</form>

<br><br><br><br>

</body>

</html>
