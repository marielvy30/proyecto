<aside class="left-sidebar" >
    <div class="scroll-sidebar">
    <div class="user-profile">
            <div class="profile-img"> <img src="/assets/images/users/2.jpg" alt="user" /> </div>
            <div class="profile-text"> <a href="/users/{{ Auth::user()->id }}">{{ Auth::user()->name }}</a>
            </div>
        </div>
        <nav class="sidebar-nav" style=" font-color: #ffffff">
            <ul id="sidebarnav">
            <!--para la vista del conductor-->
                @include('layouts.menuitem',['title'=>" Inicio",'url'=>"/home",'icon_fa'=>"fa fa-home"] )
                @include('layouts.menuitem',['title'=>" Consultar Mis Infracciones",'url'=>"/conductor/consultaXconductor",'icon_fa'=>"fa fa-user"])
                @include('layouts.menuitem',['title'=>" Impugnar Infracción",'url'=>"/conductor/ImpugnarMulta",'icon_fa'=>"fas fa-envelope-open-text"])
                @include('layouts.menuitem',['title'=>" Pagar Infraccion(es)",'url'=>"/conductor/pagoMultas",'icon_fa'=>"fa fa-credit-card"])
            </ul>
        </nav>
    </div>
</aside>
