<aside class="left-sidebar" >
    <div class="scroll-sidebar">
    <div class="user-profile">
            <div class="profile-img"> <img src="/assets/images/users/6.jpg" alt="user" /> </div>
            <div class="profile-text"> <a href="/users/">{{ Auth::guard('policia')->user()->name }}</a>
            </div>
        </div>
        <nav class="sidebar-nav" style=" font-color: #ffffff">
            <ul id="sidebarnav">

                @include('layouts.menuitem',['title'=>" Inicio",'url'=>"/homeAgente",'icon_fa'=>"fa fa-home"] )
                @include('layouts.menuitem',['title'=>" Consultar Infracción",'url'=>"/agente/consultarInfraccion",'icon_fa'=>"fa fa-user"])
                @include('layouts.menuitem',['title'=>" Aplicar Infracción",'url'=>"/agente/colocacionMultas",'icon_fa'=>"fas fa-clipboard-list"])
                @include('layouts.menuitem',['title'=>" Consultar antecedentes",'url'=>"/agente/consultarAntecedentes",'icon_fa'=>"fas fa-history"])

            </ul>
        </nav>
    </div>
</aside>


