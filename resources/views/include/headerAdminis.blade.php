<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light" style="background-color: rgb(83, 192, 238); border-color: rgb(83, 192, 238); font-color: #ffffff">
        <div class="navbar-header">

            <a class="navbar-brand" href="/">
                <img height="100px" width="200px" src="/img/digesettLogo.jpg" >
            </a>
        </div>
        <ul class="navbar-nav mr-auto mt-md-0 " style="background-color: rgb(83, 192, 238);">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" style="background-color: rgb(83, 192, 238);" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" style="background-color: rgb(83, 192, 238);" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
            </ul>
        <ul class="navbar-nav my-lg-0">
            <li class="nav-item dropdown" style="background-color: rgb(83, 192, 238);">
                <a class="nav-link dropdown-toggle text-muted waves-effect waves-green" href="/homeAdministrador">Inicio</a>
            </li>
        </ul>
        <ul class="navbar-nav my-lg-0">
            <li class="nav-item dropdown" style="background-color: rgb(83, 192, 238);">
                <a class="nav-link dropdown-toggle text-muted waves-effect waves-green" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Equipos</a>
                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                    <ul class="dropdown-user">
                        <li>
                            <div class="dw-user-box">
                                <div class="u-text">
                                    <h4>Equipos</h4>
                                </div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/administrador/crearEquipos"><i class="ti-tablet"></i> Agregar nuevo Equipo</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/administrador/AsignarEquipos"><i class="ti-link"></i> Asignar Equipos</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/administrador/consultarEquipos"><i class="ti-user"></i> Consultar Equipos disponibles</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/administrador/consultarEquiposAsignados"><i class="ti-user"></i> Consultar Equipos Asignados</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav my-lg-0">
            <li class="nav-item dropdown" style="background-color: rgb(83, 192, 238);">
                <a class="nav-link dropdown-toggle text-muted waves-effect waves-green" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Agentes</a>
                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                    <ul class="dropdown-user">
                        <li>
                            <div class="dw-user-box">

                                <div class="u-text">
                                    <h4>Agentes</h4>
                                </div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/administrador/create"><i class="fas fa-user-plus"></i> Agregar nuevo agente</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/administrador/listado"><i class="fas fa-user"></i> Consultar Agentes</a></li>
                        <li role="separator" class="divider"></li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav my-lg-0">
            <li class="nav-item dropdown" style="background-color: rgb(83, 192, 238);">
                <a class="nav-link dropdown-toggle text-muted waves-effect waves-green" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mapa</a>
                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                    <ul class="dropdown-user">
                        <li>
                            <div class="dw-user-box">
                                <div class="u-text">
                                    <h4>Mapa</h4>
                                </div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/administrador/mapa"><i class="fas fa-map-marked-alt"></i> Mapa de Equipos Activos</a></li>
                        <li><a href="/administrador/agregarUsuario"><i class="fas fa-user-plus"></i> Agregar nuevos usuarios</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown" style="background-color: rgb(83, 192, 238);">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-green" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::guard('admin')->user()->name}}  <img src="/assets/images/users/6.jpg" alt="user" class="profile-pic" /></a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"> <img src="/assets/images/users/5.jpg" alt="Usuarios DIGESETT"> </div>
                                    <div class="u-text">
                                        <h4>{{ Auth::guard('admin')->user()->name }}</h4>
                                        <p class="text-muted">{{ Auth::guard('admin')->user()->email }}</p></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a><i class="ti-user"></i> Mi Perfil</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit(); "><i class="fa fa-power-off"></i> Cerrar Sesión</a></li>
                            <form id="logout-form" action="/logout" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
    </nav>
</header>
