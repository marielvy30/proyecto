<aside class="left-sidebar" >
    <div class="scroll-sidebar">
        <div class="user-profile">
            <div class="profile-img"> <img src="/assets/images/users/5.jpg" alt="user" /> </div>
            <div class="profile-text"> <a href="/users/{{ Auth::guard('admin')->user()->id}}">{{ Auth::guard('admin')->user()->name}}</a>
            </div>
        </div>
        <nav class="sidebar-nav" style=" font-color: #ffffff">
            <ul id="sidebarnav">

                @include('layouts.menuitem',['title'=>" Inicio",'url'=>"/homeAdministrador",'icon_fa'=>"fa fa-home"] )

                <li class="nav-small-cap">Agentes</li>

                @include('layouts.menuitem',['title'=>" Registrar agente ",'url'=>"/administrador/create",'icon_fa'=>"fa fa-user-plus"])
                @include('layouts.menuitem',['title'=>" Ver Agentes",'url'=>"/administrador/listado",'icon_fa'=>"fa fa-user"])

                <li class="nav-small-cap">Equipos</li>

                @include('layouts.menuitem',['title'=>" Agregar nuevo Equipo",'url'=>"/administrador/crearEquipos",'icon_fa'=>"fas fa-clipboard-list"])
                @include('layouts.menuitem',['title'=>" Asignar Equipos",'url'=>"/administrador/AsignarEquipos",'icon_fa'=>"fas fa-clipboard-list"])
                @include('layouts.menuitem',['title'=>" Consultar Equipos disponibles",'url'=>"/administrador/consultarEquipos",'icon_fa'=>"fas fa-clipboard-list"])
                @include('layouts.menuitem',['title'=>" Consultar Equipos Asignados",'url'=>"/administrador/consultarEquiposAsignados",'icon_fa'=>"fas fa-clipboard-list"])

            </ul>
        </nav>
    </div>

</aside>
