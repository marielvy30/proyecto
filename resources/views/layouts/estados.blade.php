
<div class="form-group row">
    <label  for="{{$label_name}}" class="col-2 col-form-label">Estado: </label>
    <div class="col-10">
        <select {{isset($readonly)? "disabled":''}} class="custom-select " id="{{$label_name}}" name="{{$label_name}}">
            <option {{isset($value)? '':'selected'}}>Seleccione...</option>
            <option @if(isset($value) && $value=="Activo") {{'selected'}} @endif value="1">Activo</option>
            <option @if(isset($value) && $value=="Inactivo") {{'selected'}} @endif value="0">Inactivo</option>
        </select>
    </div>
</div>


