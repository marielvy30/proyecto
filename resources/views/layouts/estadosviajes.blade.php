
<div class="form-group row">
    <label  for="{{$label_name}}" class="col-2 col-form-label">Estado: </label>
    <div class="col-10">
        <select {{isset($readonly)? "disabled":''}} class="select2" style="width: 100%" id="{{$label_name}}" name="{{$label_name}}">

            <option @if(isset($value) && $value=="3") {{'selected'}} @endif value="3">Pendiente</option>
            <option @if(isset($value) && $value=="1") {{'selected'}} @endif value="1">Activo</option>
            <option @if(isset($value) && $value=="0") {{'selected'}} @endif value="0">Inactivo</option>
            <option @if(isset($value) && $value=="2") {{'selected'}} @endif value="2">Finalizado</option>

        </select>
    </div>
</div>


