
<div class="form-group row">
    <label  for="{{$label_name}}" class="col-2 col-form-label">{{$label}}: </label>
    <div class="col-10">
        <select {{isset($readonly)? "disabled":''}} class="select2 custom-select"
                style="visibility:hidden " {{ isset($notrequired)? "" : "required" }}  id="{{$label_name}}" name="{{$label_name}}">
            <option @if(isset($value) && $value=="") {{'selected'}} @endif value="">Seleccione</option>
            <option @if(isset($value) && $value=="Autobus") {{'selected'}} @endif value="Autobus">Autobús</option>
            <option @if(isset($value) && $value=="Automovil") {{'selected'}} @endif value="Automovil">Automóvil</option>
            <option @if(isset($value) && $value=="Camioneta") {{'selected'}} @endif value="Camioneta">Camioneta</option>
            <option @if(isset($value) && $value=="Motocicleta") {{'selected'}} @endif value="Motocicleta">Motocicleta</option>
            <option @if(isset($value) && $value=="Otro") {{'selected'}} @endif value="Otro">Otro</option>

        </select>
    </div>
</div>
