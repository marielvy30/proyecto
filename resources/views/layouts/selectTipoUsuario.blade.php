
<div class="form-group row">
    <label  for="{{$label_name}}" class="col-2 col-form-label">{{$label}}: </label>
    <div class="col-10">
        <select {{isset($readonly)? "disabled":''}} class="select2 custom-select" style="width: 100%" {{ isset($notrequired)? "" : "required" }}  id="{{$label_name}}" name="{{$label_name}}">
            <option @if(isset($value) && $value==0) {{'selected'}} @endif value="0">Conductor</option>
            <option @if(isset($value) && $value==2) {{'selected'}} @endif value="2">Agente DIGESETT</option>
            <option @if(isset($value) && $value==1) {{'selected'}} @endif value="1">Administrador</option>
        </select>
    </div>
</div>
