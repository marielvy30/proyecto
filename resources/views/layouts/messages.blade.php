@if(count($errors)>0)
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
    @endforeach

@endif

@if(session('success') || isset($success_msg))
    <div class="alert alert-success alert-rounded"> {{isset($success_msg)? $success_msg:session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
@endif