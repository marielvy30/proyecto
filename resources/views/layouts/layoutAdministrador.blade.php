<!DOCTYPE html>
<html lang="en">
@include('include.headAd')
<body class="fix-header card-no-border">
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<div id="main-wrapper">
    @include('include.headerAdminis')
    @include('include.sidebarAdministrador')
    <div class="page-wrapper">
        <div class="container-fluid">
            @yield('content')
        </div>
        @include('include.footerAdministrador')
    </div>
</div>
@include('include.scripts')
@yield('scripts')
</body>
</html>
