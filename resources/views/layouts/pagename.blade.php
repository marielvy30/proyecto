<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{$title}}</h3>
        @if(isset($name))
            @if($name!=="Inicio")
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                    <li class="breadcrumb-item active">{{$name}}</li>
                </ol>
            @endif
        @endif
    </div>
    @if(isset($page))
        <div class="col-md-6 col-4 align-self-center">
            <a href="/administrador/create" class="btn pull-right btn-success" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);"><i class="mdi mdi-plus-circle"></i> Crear Nuevo Agente</a>
        </div>
    @endif
</div>
