
<div class="form-group row">
    <label  for="{{$label_name}}" class="col-2 col-form-label">{{$label}}: </label>
    <div class="col-10">
        <select {{isset($readonly)? "disabled":''}} class="select2 custom-select" style="width: 100%" {{ isset($notrequired)? "" : "required" }}  id="{{$label_name}}" name="{{$label_name}}">
            <option @if(isset($value) && $value=="O+") {{'selected'}} @endif value="O+">O+</option>
            <option @if(isset($value) && $value=="O-") {{'selected'}} @endif value="O-">O-</option>
            <option @if(isset($value) && $value=="A+") {{'selected'}} @endif value="A+">A+</option>
            <option @if(isset($value) && $value=="A-") {{'selected'}} @endif value="A-">A-</option>
            <option @if(isset($value) && $value=="B+") {{'selected'}} @endif value="B+">B+</option>
            <option @if(isset($value) && $value=="B-") {{'selected'}} @endif value="B-">B-</option>
            <option @if(isset($value) && $value=="AB+") {{'selected'}} @endif value="AB-">AB+</option>
            <option @if(isset($value) && $value=="AB-") {{'selected'}} @endif value="AB-">AB-</option>

        </select>
    </div>
</div>
