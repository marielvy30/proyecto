
<div class="form-group row">
    <label  for="{{$label_name}}" class="col-2 col-form-label">{{$label}}: </label>
    <div class="col-10">
        <select {{isset($readonly)? "disabled":''}} class="select2 custom-select" style="width: 100%" {{ isset($notrequired)? "" : "required" }}  id="{{$label_name}}" name="{{$label_name}}">
            <option @if(isset($value) && $value=="Masculino") {{'selected'}} @endif value="Masculino">Masculino</option>
            <option @if(isset($value) && $value=="Femenino") {{'selected'}} @endif value="Femenino">Femenino</option>
        </select>
    </div>
</div>
