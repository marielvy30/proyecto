
<div class="form-group row">
    <label  for="{{$label_name}}" class="col-2 col-form-label">{{$label}}: </label>
    <div class="col-10">
        <select {{isset($readonly)? "disabled":''}} class="select2 custom-select" style="width: 100%" {{ isset($notrequired)? "" : "required" }}  id="{{$label_name}}" name="{{$label_name}}">
            <option @if(isset($value) && $value=="01") {{'selected'}} @endif value="02">Matutina</option>
            <option @if(isset($value) && $value=="02") {{'selected'}} @endif value="01">Vespertina</option>
        </select>
    </div>
</div>
