@extends('layouts.layoutConductor')

@section('page')
    Método de pago
@endsection
@section('content')
    @if(Auth::user()->tipo==1)
        @include('layouts.pagename',['name'=>'Pagar Infracción','title'=>'Pago infracción'])
        @include('layouts.messages')
        <form action="{{url('/conductor/ApelarMultas')}}" method="post">
            @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        Infraccion(es) que puede apelar.
                    </div>
                    <div class="card-body">

                            @if(count($multas) > 0)

                                <div class="table-responsive">
                                    <table id="datatable" class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                                        <thead>
                                        <th>Infracción</th>
                                        <th>Nombre Conductor</th>
                                        <th>Agente</th>
                                        <th>Zona</th>
                                        <th>Detalle</th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                        <th>¿Cuál(es) desea apelar?<th>
                                        </thead>
                                        <tbody>
                                        @foreach($multas as $multa)
                                            @if($multa->estado == "pendiente" && $diasDiferencia<=30 )
                                                <tr>
                                                    <td>{{$multa->id}}</td>
                                                    <td>{{$multa->user->name}}</td>
                                                    <td>{{$multa->policia->user->name}}</td>
                                                    <td>{{$multa->zonaMulta()}}</td>
                                                    <td>{{$multa->descripcion}}</td>
                                                    <td>{{$multa->estado}}</td>
                                                    <td>{{$multa->created_at->format('d/M/Y')}}</td>
                                                    <td width="30%">
                                                        <div class="row ">
                                                            <div class="col-md-6 text-center options">
                                                                <label for="apela">
                                                                    <input type="checkbox" id="apela" value="{{$multa->id}}" name="seleccionApelacion[]" required="required">
                                                                    Apelar
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @elseif(count($multas) == 0)
                                No tiene multas pendiente por pagar
                                <button type="submit" name="pagar" id="pagar" style="display: none" class="btn btn-primary btn-block">Enviar Apelación</button>
                            @endif

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Alegaciones
                    </div>
                    <div class="card-body">
                        <hr>

                        <div class="form-group row">

                                <div class="col-md-6 option">
                                    <label for="apela">
                                        <input id="alegacion1" class="form-control" name='alegacion1'  type="hidden" value=""/>
                                        <input type="checkbox" id="apela" value="1" name="seleccionAlegacion[]" required="required">
                                        Hechos denunciados no son ciertos o exactos.
                                    </label>
                                </div>

                        </div>

                        <div class="form-group row">

                                <div class="col-md-6 option">
                                    <label for="apela">
                                        <input id="alegacion2" class="form-control" name='alegacion2'  type="hidden"
                                               value=""/>
                                        <input type="checkbox" id="apela" value="2" name="seleccionAlegacion[]" required="required">
                                        Inconcreción
                                    </label>
                                </div>

                        </div>

                        <div class="form-group row">

                                <div class="col-md-6 option">
                                    <label for="apela">
                                        <input id="alegacion3" class="form-control" name='alegacion3'  type="hidden"
                                               value=""/>
                                        <input type="checkbox" id="apela" value="3" name="seleccionAlegacion[]" required="required">
                                        Defectos en la forma de la denuncia.
                                    </label>
                                </div>
                        </div>
                        <div class="form-group row">
                                <div class="col-md-6 option">
                                    <label for="apela">
                                        <input id="alegacion4" class="form-control" name='alegacion4'  type="hidden"
                                               value=""/>
                                        <input type="checkbox" id="apela" value="4" name="seleccionAlegacion[]" required="required">
                                        Falta de datos o datos erróneos.
                                    </label>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Solicitud de pruebas.
                        </div>
                        <div class="card-body">
                            <hr>
                            <div class="form-group row">

                                <div class="col-md-6 optiones">
                                    <label for="solicitud">
                                        <input id="solicitud1" class="form-control" name='solicitud1'  type="hidden"
                                               value=""/>
                                        <input type="checkbox" id="solicitud" value="1" name="seleccionSolicitud[]" required="required">
                                        Multas de rádar.
                                    </label>
                                </div>

                            </div>
                            <div class="form-group row">

                                <div class="col-md-6 optiones">
                                    <label for="solicitud">
                                        <input id="solicitud2" class="form-control" name='solicitud2'  type="hidden"
                                               value=""/>
                                        <input type="checkbox" id="solicitud" value="2" name="seleccionSolicitud[]" required="required">
                                        Multa por alcoholemia.
                                    </label>
                                </div>

                            </div>

                            <div class="form-group row">
                                    <div class="col-md-6 optiones">
                                        <label for="solicitud">
                                            <input id="solicitud3" class="form-control" name='solicitud3'  type="hidden"
                                                   value=""/>
                                            <input type="checkbox" id="solicitud" value="3" name="seleccionSolicitud[]" required="required">
                                            Multa semáforo en rojo.
                                        </label>
                                    </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-md-6 optiones">
                                    <label for="solicitud">
                                        <input id="solicitud4" class="form-control" name='solicitud4'  type="hidden"
                                               value=""/>
                                        <input type="checkbox" id="solicitud" value="4" name="seleccionSolicitud[]" required="required">
                                       No.
                                    </label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



        @if($pendiente === 0  )
            <button type="submit" name="apelar" id="apelar" style="display: none" class="btn btn-primary btn-block">Enviar Apelación</button>
        @elseif($pendiente > 0  )
            <button type="submit" name="apelar" id="apelar"style="background-color: rgb(25, 101, 185); border:rgb(25, 101, 185)" class="btn btn-primary btn-block">Enviar Apelación</button>
            @endif
        </form>
    @elseif(Auth::user()->tipo==2)
        echo "<script> window.location= '/homeAgente' </script>";
    @elseif(Auth::user()->tipo==3)
        echo "<script> window.location= '/homeAdministrador' </script>";
    @endif

@endsection

@push('extra_scripts')
    <script>
        $(function(){
            var requiredCheckboxes = $('.options :checkbox[required]');
            requiredCheckboxes.change(function(){
                if(requiredCheckboxes.is(':checked')) {
                    requiredCheckboxes.removeAttr('required');
                }
                else
                {
                    requiredCheckboxes.attr('required', 'required');
                }
            });

            var requiredCheckboxe = $('.option :checkbox[required]');
            requiredCheckboxe.change(function(){
                if(requiredCheckboxe.is(':checked')) {
                    requiredCheckboxe.removeAttr('required');
                }
                else
                {
                    requiredCheckboxe.attr('required', 'required');
                }
            });
            var requiredCheckbox = $('.optiones :checkbox[required]');
            requiredCheckbox.change(function(){
                if(requiredCheckbox.is(':checked')) {
                    requiredCheckbox.removeAttr('required');
                }
                else
                {
                    requiredCheckbox.attr('required', 'required');
                }
            });

        });

    </script>
    <script>
        $(document).ready(function() {

            $('[name="seleccionAlegacion[]"]').click(function() {

                var arrAle = $('[name="seleccionAlegacion[]"]:checked').map(function(){
                    return this.value;
                }).get();
                var strAle = arrAle.join(',');
                $('#arrAle').text(JSON.stringify(arrAle));
                $('#strAle').text(strAle);
                text1 ="ALEGACIÓN: Que se interpongan de sus buenos oficios, a los fines de que me sean levantadas las multas descritas en la relación de multa anexas a la presente instancia, ya que los hechos no ocurrieron como lo detallan los agentes de SIMV.";
                text2="ALEGACIÓN: La denuncia se limita a señalar el incumplimiento de un precepto de forma general, sin aludir a hechos concretos que concreten la infracción, causando así una grave indefensión al interesado, al no poder formular alegaciones de forma adecuada.";
                text3="ALEGACIÓN: La denuncia notificada incurre en un vicio insubsanable de invalidez, como norma general las denuncias se han de notificar en el acto al denunciado. En el presente caso, en la notificación de denuncia remitida no consta ninguna causa concreta y específica por la que no se notificó en el acto la denuncia o bien, los motivos que constan no son ciertos o no son suficientemente específicos, tal como exige la jurisprudencia.";
                text4="ALEGACIÓN: Falta (o es erróneo) algunos de los datos que la denuncia debe contener, conforme a los apartados: identificación del vehículo, identidad del denunciado, relación circunstanciada del hecho y número de identificación del Agente.";
                switch (strAle) {
                    case '1':
                        console.log("Alegacion: %s",text1);
                        $("#alegacion1").val(text1);
                        $("#alegacion2").val("");
                        $("#alegacion3").val("");
                        $("#alegacion4").val("");
                        break;
                    case '2':
                        console.log("alegacion: %s",text2);
                        $("#alegacion2").val(text2);
                        $("#alegacion1").val("");
                        $("#alegacion3").val("");
                        $("#alegacion4").val("");
                        break;
                    case '3':
                        console.log("alegacion: %s",text3);
                        $("#alegacion3").val(text3);
                        $("#alegacion1").val("");
                        $("#alegacion2").val("");
                        $("#alegacion4").val("");

                        break;
                    case '4':
                        console.log("alegacion: %s",text4);
                        $("#alegacion4").val("");
                        $("#alegacion1").val("");
                        $("#alegacion3").val("");
                        $("#alegacion4").val(text4);
                        break;
                    case '1,2':

                        console.log("Alegacion: %s %s",text1, text2);
                        $("#alegacion1").val(text1);
                        $("#alegacion2").val(text2);
                        $("#alegacion3").val("");
                        $("#alegacion4").val("");
                        break;
                    case '1,3':

                        console.log("Alegacion: %s %s",text1, text3);
                        $("#alegacion1").val(text1);
                        $("#alegacion2").val("");
                        $("#alegacion3").val(text3);
                        $("#alegacion4").val("");
                        break;
                    case '1,4':

                        console.log("Alegacion: %s %s",text1, text4);
                        $("#alegacion1").val(text1);
                        $("#alegacion2").val("");
                        $("#alegacion3").val("");
                        $("#alegacion4").val(text4);
                        break;
                    case '2,3':

                        console.log("Alegacion: %s %s",text2, text3);
                        $("#alegacion1").val("");
                        $("#alegacion2").val(text2);
                        $("#alegacion3").val(text3);
                        $("#alegacion4").val("");
                        break;
                    case '2,4':

                        console.log("Alegacion: %s %s",text2, text4);
                        $("#alegacion1").val("");
                        $("#alegacion2").val(text2);
                        $("#alegacion3").val("");
                        $("#alegacion4").val(text4);
                        break;
                    case '3,4':
                        console.log("Alegacion: %s %s",text3, text4);
                        $("#alegacion1").val("");
                        $("#alegacion2").val("");
                        $("#alegacion3").val(text3);
                        $("#alegacion4").val(text4);
                        break;
                    case '1,2,3':

                        console.log("Alegacion: %s %s %s",text1, text2, text3);
                        $("#alegacion1").val(text1);
                        $("#alegacion2").val(text2);
                        $("#alegacion3").val(text3);
                        $("#alegacion4").val("");
                        break;
                    case '1,2,4':

                        console.log("Alegacion: %s %s %s",text1, text2, text4);
                        $("#alegacion1").val(text1);
                        $("#alegacion2").val(text2);
                        $("#alegacion3").val("");
                        $("#alegacion4").val(text4);
                        break;
                    case '1,3,4':

                        console.log("Alegacion: %s %s %s",text1, text3, text4);
                        $("#alegacion1").val(text1);
                        $("#alegacion2").val("");
                        $("#alegacion3").val(text3);
                        $("#alegacion4").val(text4);
                        break;
                    case '2,3,4':

                        console.log("Alegacion: %s %s %s",text2, text3, text4);
                        $("#alegacion1").val("");
                        $("#alegacion2").val(text2);
                        $("#alegacion3").val(text3);
                        $("#alegacion4").val(text4);
                        break;
                    case '1,2,3,4':

                        console.log("Alegacion: %s %s %s",text1,text2, text3, text4);
                        $("#alegacion1").val(text1);
                        $("#alegacion2").val(text2);
                        $("#alegacion3").val(text3);
                        $("#alegacion4").val(text4);
                        break;

                }
            });

            $('[name="seleccionSolicitud[]"]').click(function() {
                var arrSoli = $('[name="seleccionSolicitud[]"]:checked').map(function(){
                    return this.value;
                }).get();
                var strSoli = arrSoli.join(',');
                $('#arrSoli').text(JSON.stringify(arrSoli));
                $('#strSoli').text(strSoli);
                //console.log("Solicitud: %O", strSoli);
                textS1 ="SOLICITUD: Certificaciones expedidas por el Centro Español de Metrología con expresión de marca y modelo del cinemómetro utilizado, número del equipo, fecha de aprobación del aparato, fecha de revisión anual y período de validez, fecha de la última revisión tras reparación posterior, márgenes de error tolerados. Foto realizada por el radar donde se aprecie mi vehículo.\n";
                textS2="SOLICITUD: Tickets obtenidos en la prueba de detección alcohólica. Certificado del organismo competente que acredite que el etilómetro ha superado los controles legales, con indicación de los márgenes de error y cómo se han aplicado.";
                textS3="SOLICITUD: Foto realizada por el semáforo donde se aprecie mi vehículo. Acreditación del control del sistema de control fotográfico que constató la supuesta infracción que garantice su adecuado funcionamiento, estado, validez y verificación desde el punto de vista metrológico.";
                textS4=" ";

                switch (strSoli) {
                    case '1':
                        console.log("solicitud: %s",textS1);
                        $("#solicitud1").val(textS1);
                        $("#solicitud2").val("");
                        $("#solicitud3").val("");
                        $("#solicitud4").val("");
                        break;
                    case '2':
                        console.log("solicitud: %s",textS2);
                        $("#solicitud2").val(textS2);
                        $("#solicitud1").val("");
                        $("#solicitud3").val("");
                        $("#solicitud4").val("");
                        break;
                    case '3':
                        console.log("solicitud: %s",textS3);
                        $("#solicitud3").val(textS3);
                        $("#solicitud1").val("");
                        $("#solicitud2").val("");
                        $("#solicitud4").val("");

                        break;
                    case '4':
                        console.log("solicitud: %s",textS4);
                        $("#solicitud4").val("");
                        $("#solicitud1").val("");
                        $("#solicitud3").val("");
                        $("#solicitud4").val(textS4);
                        break;
                    case '1,2':

                        console.log("solicitud: %s %s",textS1, textS2);
                        $("#solicitud1").val(textS1);
                        $("#solicitud2").val(textS2);
                        $("#solicitud3").val("");
                        $("#solicitud4").val("");
                        break;
                    case '1,3':

                        console.log("solicitud: %s %s",textS1, textS3);
                        $("#solicitud1").val(textS1);
                        $("#solicitud2").val("");
                        $("#solicitud3").val(textS3);
                        $("#solicitud4").val("");
                        break;
                    case '1,4':

                        console.log("solicitud: %s %s",textS1, textS4);
                        $("#solicitud1").val(textS1);
                        $("#solicitud2").val("");
                        $("#solicitud3").val("");
                        $("#solicitud4").val(textS4);
                        break;
                    case '2,3':

                        console.log("solicitud: %s %s",textS2, textS3);
                        $("#solicitud1").val("");
                        $("#solicitud2").val(textS2);
                        $("#solicitud3").val(textS3);
                        $("#solicitud4").val("");
                        break;
                    case '2,4':

                        console.log("solicitud: %s %s",textS2, textS4);
                        $("#solicitud1").val("");
                        $("#solicitud2").val(textS2);
                        $("#solicitud3").val("");
                        $("#solicitud4").val(textS4);
                        break;
                    case '3,4':
                        console.log("Alegacion: %s %s",textS3, textS4);
                        $("#solicitud1").val("");
                        $("#solicitud2").val("");
                        $("#solicitud3").val(textS3);
                        $("#solicitud4").val(textS4);
                        break;
                    case '1,2,3':

                        console.log("solicitud: %s %s %s",textS1, textS2, textS3);
                        $("#solicitud1").val(textS1);
                        $("#solicitud2").val(textS2);
                        $("#solicitud3").val(textS3);
                        $("#solicitud4").val("");
                        break;
                    case '1,2,4':

                        console.log("solicitud: %s %s %s",textS1, textS2, textS4);
                        $("#solicitud1").val(textS1);
                        $("#solicitud2").val(textS2);
                        $("#solicitud3").val("");
                        $("#solicitud4").val(textS4);
                        break;
                    case '1,3,4':

                        console.log("solicitud: %s %s %s",textS1, textS3, textS4);
                        $("#solicitud1").val(textS1);
                        $("#solicitud2").val("");
                        $("#solicitud3").val(textS3);
                        $("#solicitud4").val(textS4);
                        break;
                    case '2,3,4':

                        console.log("solicitud: %s %s %s",textS2, textS3, textS4);
                        $("#solicitud1").val("");
                        $("#solicitud2").val(textS2);
                        $("#solicitud3").val(textS3);
                        $("#solicitud4").val(textS4);
                        break;
                    case '1,2,3,4':

                        console.log("solicitud: %s %s %s",textS1,textS2, textS3, textS4);
                        $("#solicitud1").val(textS1);
                        $("#solicitud2").val(textS2);
                        $("#solicitud3").val(textS3);
                        $("#solicitud4").val(textS4);
                        break;

                }


            });
        });

    </script>


    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {


                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "No tiene multas pendientes por pagar",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
        });
    </script>

@endpush
