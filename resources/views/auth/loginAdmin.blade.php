<!DOCTYPE html>
<html lang="en" class="loginbody img-responsive" style="height: 400px ; width: 400px;">

<head >
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/img/digesettLogo.jpg')}}">
    <title>Login Admin SIMV</title>

    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}} " rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" style="--background-image:url({{url('/img/fondo2.jpg')}});">
    <link href="{{asset('assets/css/colors/green-dark.css')}}" id="theme" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
</head>
<body  class="loginbody">
<section id="wrapper">

    <div class="login-register"  >
        <div class="login-box card">
            <div class="card-body">
                <form method="POST" class="form-horizontal form-material" id="loginform" action="/login/admin">
                    @csrf
                    <h3 class="box-title mb-3"> <center> <img height="100px" width="350px" src="/img/digesettLogo.jpg" align="middle"> </center> </h3>
                    <h3 class="box-title mb-3"> <center><img height="100px" width="200px" src="/img/loginAd.jpg" align="middle"></center></h3>
                    <h3 class="box-title mb-3"> <center>Administrador</center></h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input id="email" type="email" placeholder="Email"  class="form-control  {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input id="password" type="password" placeholder="Contraseña" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary float-left pt-0">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember"> Recuerdame </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center mt-3">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" style="background-color: rgb(25, 101, 185);" type="submit">Iniciar Sesión</button>
                        </div>
                    </div>

                    <div class="form-group mb-0">
                        <div class="col-sm-12 text-center">
                            <p><a href="" class="text-info ml-1"><b>¿Olvidaste la contraseña?</b></a></p>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>

<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/sidebarmenu.js')}}"></script>
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('assets/js/custom.min.js')}}"></script>
<script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
</body>
</html>
