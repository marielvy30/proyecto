@extends('layouts.layoutAdministrador')

@section('page')
    Inicio
@endsection

@section('content')
    @include('layouts.pagename',['name'=>'Tablero','title'=>'Tablero'])

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Inicio
                </div>
                <div class="card-body">
                    <h5 class="card-title">Principales</h5>
                    <div class="row">
                        <div class="col-ms-6 col-xlg-3">
                            <div class="card  card-blue">
                                <div class="box text-center" width="200px" height="200px">
                                    <img width="100px" height="100px" src="/img/equipos.png" alt="img" />
                                    <a href="/administrador/crearEquipos" class="btn btn-secondary">Agrega Nuevo Equipo</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-ms-8 col-xlg-3">
                            <div class="card card-inverse card-blue">
                                <div class="box text-center">
                                    <img width="100px" height="100px" src="/img/agent.png" alt="img" />
                                    <a href="/administrador/create" class="btn btn-secondary">Agregar Nuevo Agente</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-ms-6 col-xlg-3">
                            <div class="card card-inverse card-blue">
                                <div class="box text-center">
                                    <img width="100px" height="100px" src="/img/mapa.png" alt="img" />
                                    <a href="/administrador/mapa" class="btn btn-secondary">Equipos Activos</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>

                        <div class="col-ms-6 col-xlg-3">
                            <div class="card card-inverse card-blue">
                                <div class="box text-center">
                                    <img width="100px" height="100px" src="/img/asignar.png" alt="img" />
                                    <a href="/administrador/AsignarEquipos" class="btn btn-secondary">Asignar Equipos</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
