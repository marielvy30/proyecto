@extends('layouts.layoutAdministrador')
@section('page')
    Agregar Nuevos Equipos
@endsection

@section('content')
    @include('layouts.pagename',['title'=>'Agregar Equipos','name'=>'Nuevo Equipo'])
    @include('layouts.messages')
    <form action="{{url('/guardarEquipo')}}" method="post">
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Datos del nuevo equipo
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="tipo_equipo" class="col-2 col-form-label">
                                Tipo Equipo:
                            </label>
                            <div class="col-10">
                                <select class="select2 custom-select" required="" onchange="pregunta()" id="tipo_equipo" name="tipo_equipo">
                                    <option value="">Seleccione</option>
                                    <option @if(isset($value) && $value=="tablet") {{'selected'}} @endif value="tablet">Tablet SIMV</option>
                                    <option @if(isset($value) && $value=="impresora") {{'selected'}} @endif value="impresora">Lector</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">
                                Nombre :
                            </label>
                            <div class="col-10">
                                <input class="form-control" maxlength="7" autocomplete="off" style="display:inline" disabled="disabled" id="nombre" name="nombre" required="" value="">
                                <input class="form-control" maxlength="7"  autocomplete="off" style="display:none"  disabled="disabled" id="nombre_imp" name="nombre_imp" required="" value="lec-{{$counter = count($prints)+1}}">
                                <input class="form-control" maxlength="7" autocomplete="off" style="display:none" disabled="disabled" id="nombre_tablet" name="nombre_tablet" required="" value="tab-{{$counte = count($equipos)+1}}">
                                <input class="form-control" maxlength="7"  autocomplete="off" style="display:none"   id="nombre_i" name="nombre_i" required="" value="lec-{{$counter = count($prints)+1}}">
                                <input class="form-control" maxlength="7" autocomplete="off" style="display:none"  id="nombre_t" name="nombre_t" required="" value="tab-{{$counte = count($equipos)+1}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="serial" class="col-2 col-form-label">
                                Serial:
                            </label>
                            <div class="col-10">
                                <input class="form-control" autocomplete="off" type="text" maxlength="11" id="serial" required=""name="serial" value="">
                            </div>
                        </div>
                        <input id="estado" type="hidden" class="form-control" name="estado" maxlength="11"  value="inactivo">
                        <button type="submit" name="equipo" id="equipo" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);" class="btn btn-primary btn-block">Crear nuevo equipo</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
<script>
    function pregunta() {
        if (document.getElementById('tipo_equipo').value === "tablet")
        {
            jQuery('input[name="nombre_tablet"]').prop('style', 'display:inline');
            jQuery('input[name="nombre_imp"]').prop('style', 'display:none');
            jQuery('input[name="nombre"]').prop('style', 'display:none');
        }
        if (document.getElementById('tipo_equipo').value === "impresora")
        {
            jQuery('input[name="nombre_tablet"]').prop('style', 'display:none');
            jQuery('input[name="nombre_imp"]').prop('style', 'display:inline');
            jQuery('input[name="nombre"]').prop('style', 'display:none');
        }
        if (document.getElementById('tipo_equipo').value === "")
        {
            jQuery('input[name="nombre_tablet"]').prop('style', 'display:none');
            jQuery('input[name="nombre_imp"]').prop('style', 'display:none');
            jQuery('input[name="nombre"]').prop('style', 'display:inline');
        }
    }
</script>
@endsection
