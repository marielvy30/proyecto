@extends('layouts.layoutAdministrador')
@section('page')
    Consulta Equipos Disponibles
@endsection

@section('content')
    @include('layouts.pagename',['title'=>'consultar equipo disponible','name'=>'consulta equipo disponible'])
    @include('layouts.messages')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Listado Equipos Disponibles
                </div>
                <div class="card-body">
                    <div class="table-responsive">

                        <table id="datatable" class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                            <thead>
                            <tr>
                                <th>Tipo Equipo</th>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Serial</th>
                                <th>Estado</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(count($elementos) > 0  )
                                @foreach($elementos as $elemento)
                                    <tr>
                                        <td>Tablet SIMV</td>
                                        <td>{{$elemento->id}}</td>
                                        <td>{{$elemento->nombre}}</td>
                                        <td>{{$elemento->serial}}</td>
                                        <td>{{$elemento->estado}}</td>

                                    </tr>
                                @endforeach
                            @endif
                            @if(count($elements) > 0  )
                                @foreach($elements as $elemento)
                                    <tr>
                                        <td>Impresora</td>
                                        <td>{{$elemento->id}}</td>
                                        <td>{{$elemento->nombre}}</td>
                                        <td>{{$elemento->serial}}</td>
                                        <td>{{$elemento->estado}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('extra_scripts')
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {


                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "No hay equipos disponibles",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
        });
    </script>

@endpush
