@extends('layouts.layoutAdministrador')
@section('page')
    Búsqueda Agente
@endsection

@section('content')
    @include('layouts.pagename',['title'=>$pluralModel,'name'=>$pluralModel,'page'=>strtolower($pluralModel)])
    @include('layouts.messages')
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Búsqueda por ID
                    </div>
                    <div class="card-body">
                        <form>
                            @csrf
                            <div class="row">
                                <div class="col-md-10">
                                    <input id="id" type="text" class="form-control" name='id'
                                           placeholder="ID ..."  value="{{isset($usuarioEncon) && $usuarioEncon ? $usuario->id:''}}"/>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);" class="btn btn-success">Buscar...</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @if(isset($usuarioEncon) && $usuarioEncon)
                        Agente
                    @elseif(isset($usuarioEncon) && !$usuarioEncon)
                        El ID buscado no existe.
                    @else
                        Agentes
                    @endif
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                            <thead>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            </thead>
                            @if(isset($usuarioEncon) && $usuarioEncon)
                                <tbody>
                                <tr>
                                    <td>{{$usuario->id}}</td>
                                    <td>{{$usuario->name}}</td>
                                    <td>{{$usuario->email}}</td>

                                </tr>

                                </tbody>
                            @elseif(isset($usuarioEncon) && !$usuarioEncon)
                                <tbody>
                                <tr>
                                    <td>El agente no existe</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            @else
                                Agente
                            @endif

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('extra_scripts')
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Agente no encontrado",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
        });
    </script>
@endpush
