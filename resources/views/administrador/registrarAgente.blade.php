@extends('layouts.plainLayout')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">


                    <div class="card-body">

                        <div class="row">
                            <h3>Formulario de Registro</h3>

                        </div>

                        <form action="{{url('/buscarUsuario')}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-10">
                                    <input id="cedula" type="text" class="form-control" name='cedula'
                                           maxlength="11" placeholder="Cédula ..." value="{{isset($usuarioEncontrado) && $usuarioEncontrado ? $usuario->cedula:''}}"/>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-success">Buscar...</button>
                                </div>
                            </div>
                        </form>

                        @if(isset($usuarioEncontrado) && $usuarioEncontrado && !$usuario->registrado)
                            <hr>
                            <div class="row justify-content-center">
                                <h4>Usuario Encontrado</h4>
                            </div>

                            <form action="{{url('/guardarUsuario?id='.$usuario->id)}}" method="post">
                                @csrf

                                @include('layouts.textfield',['obj_id'=>'name',"label_name"=>"Nombres","readonly"=>"" , "value"=>$usuario->name])
                                @include('layouts.textfield',['obj_id'=>'email',"type"=>"email","required"=>"","label_name"=>"Correo", "value"=>$usuario->email ])
                                @include('layouts.textfield',['obj_id'=>'password',"type"=>"password","required"=>"","label_name"=>"Contraseña", "value"=>$usuario->password ])
                                @include('layouts.textfield',["required"=>"",'obj_id'=>'cargo',"label_name"=>"Fecha Nacimiento", "value"=>$usuario->fecha_nacimiento])
                                @include('layouts.selectSexo',['label_name'=>'sexo',"label"=>"Sexo" ])
                                @include('layouts.textfield',["required"=>"",'obj_id'=>'celular',"label_name"=>"Celular/Teléfono", "value"=>$usuario->celular])
                                @include('layouts.textfield',["required"=>"",'obj_id'=>'direccion',"label_name"=>"Dirección", "value"=>$usuario->direccion ])

                                <div class="form-group row">
                                    <div class="col-10">
                                        <input class="form-control" type="hidden" id="registrado" name="registrado" required="" value="1">
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-10">
                                        <input class="form-control" type="hidden" id="tipo" name="tipo" required="" value="1">
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-primary">Guardar</button>


                            </form>
                        @elseif(isset($usuarioEncontrado) && $usuarioEncontrado && $usuario->registrado)
                            <hr>
                            <div class="row justify-content-center">
                                <h4>Usuario Ya Registrado</h4>
                            </div>
                        @elseif(isset($usuarioEncontrado) && !$usuarioEncontrado)
                            <hr>
                            <div class="row justify-content-center">
                                <h4>Usuario No Encontrado</h4>
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
