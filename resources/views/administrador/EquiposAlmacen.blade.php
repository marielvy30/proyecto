@extends('layouts.layoutAdministrador')
@section('page')
    Consulta Equipos
@endsection

@section('content')
    @include('layouts.pagename',['title'=>'consulta equipo','name'=>'consulta equipo'])
    @include('layouts.messages')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">

                        <table id="datatable" class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                            <thead>
                            <tr>
                                <th>Tipo Equipo</th>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Serial</th>
                                <th>Estado</th>
                                <th>Observacion</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(count($elementos) > 0  )
                                @foreach($elementos as $elemento)
                                    <tr>
                                        <td>Tablet SIMV</td>
                                        <td>{{$elemento->id}}</td>
                                        <td>{{$elemento->nombre}}</td>
                                        <td>{{$elemento->serial}}</td>
                                        <td>{{$elemento->estado}}</td>
                                        <td>{{$elemento->obsevacion}}</td>

                                    </tr>
                                @endforeach
                            @endif
                            @if(count($elements) > 0  )
                                @foreach($elements as $elemento)
                                    <tr>
                                        <td>Impresora</td>
                                        <td>{{$elemento->id}}</td>
                                        <td>{{$elemento->nombre}}</td>
                                        <td>{{$elemento->serial}}</td>
                                        <td>{{$elemento->estado}}</td>
                                        <td>{{$elemento->obsevacion}}</td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('extra_scripts')
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            } );
        });
    </script>
@endpush
