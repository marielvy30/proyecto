@extends('layouts.layoutAdministrador')
@section('page')
    Asignación de Equipos
@endsection

@section('content')
    @include('layouts.pagename',['title'=>'Asignación Equipos','name'=>'Asignar Equipo'])
    @include('layouts.messages')
    @inject('equipos', 'App\Services\Equipos')
    @inject('impresoras', 'App\Services\Impresoras')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Búsqueda del agente (ID).
                </div>
                <div class="card-body">
                    <form>
                        @csrf
                        <div class="row">
                            <div class="col-md-10">
                                <input id="id" type="text" class="form-control" name='id'
                                       placeholder="ID ..."  value="{{isset($usuarioEncon) && $usuarioEncon ? $usuario->id:''}}"/>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);" class="btn btn-success">Buscar...</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <form action="{{url('/AsignacionEquipos')}}" method="post">
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Asignación de los equipos
                    </div>
                    <div class="card-body">
                        @if(isset($usuarioEncon) && $usuarioEncon)
                            <div class="form-group row">
                                <label for="id" class="col-2 col-form-label"> ID Agente :</label>
                                <div class="col-10">
                                    <input class="form-control" autocomplete="off" disabled="disabled" type="text" id="id-u" required="" name="id-u" value="{{$usuario->id}}">
                                    <input class="form-control" type="hidden" id="id" required="" name="id" value="{{$usuario->id}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="nombre" class="col-2 col-form-label"> Nombre Agente:</label>
                                <div class="col-10">
                                    <input class="form-control" autocomplete="off" disabled="disabled" type="text" id="nombre" required="" name="nombre" value="{{$usuario->name}}">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="tablet" style='' name="tabletd" class="col-2 col-form-label">Tablet SIMV</label>
                                <div class="col-10">
                                    <select id="tablet" name="tablet_id" style='' class="form-control{{ $errors->has('tablet_id') ? ' is-invalid' : '' }}">
                                        @foreach($equipos->get() as $index => $equipo)
                                            <option value="{{ $index }}" {{ old('equipo_id') == $index ? 'selected' : '' }}>
                                                {{ $equipo }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="impresora" style='' name="impresorad" class="col-2 col-form-label">Lector</label>
                                <div class="col-10">
                                    <select id="impresora" name="impresora_id" style='' class="form-control{{ $errors->has('impresora_id') ? ' is-invalid' : '' }}">
                                        @foreach($impresoras->get() as $index => $impresora)
                                            <option value="{{ $index }}" {{ old('impresora_id') == $index ? 'selected' : '' }}>
                                                {{ $impresora }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        @elseif(isset($usuarioEncon) && !$usuarioEncon)
                            El ID buscado no existe.
                        @else
                            Infracciones
                        @endif


                    </div>
                </div>
            </div>
            <button type="submit" name="equipo" id="equipo" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);" class="btn btn-primary btn-block">Asignar equipo</button>
        </div>
    </form>

@endsection
