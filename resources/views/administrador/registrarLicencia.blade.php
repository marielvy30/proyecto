@extends('layouts.layoutAdministrador')

@section('page')
    Home
@endsection

@section('content')
    @include('layouts.pagename',['name'=>'Registrar Licencia','title'=>'Acta de Licencia'])

    <link rel="stylesheet" href="css/colocacionMultas.css">
    <label id="buscaramet" for="buscaramet">Agente DIGESETT:</label>
    <input type="text" class="form-control" id="buscaramet" name="policia_id" autocomplete="off"  placeholder="" onchange="onBuscarAmet()">



    <div class="card-body">



        <div class="form-row">
            <div class="form-group col-md-3">
                <label>Buscar Persona:</label>
                <input type="text" class="form-control" maxlength="17" onchange="onCedulaChange()" autocomplete="off" name="busqueda" id="busqueda" placeholder=""  onkeypress='return event.charCode>=48 && event.charCode<=57'>

            </div>

            <div class="form-group col-md-3">
                <label for="primernombre">Nombres:</label>
                <input type="text" class="form-control" id="primernombre" disabled="disabled" autocomplete="off" placeholder="">
            </div>

            <div class="form-group col-md-3">
                <label for="primerapellido">Primer apellido:</label>
                <input type="text" class="form-control" id="primerapellido" autocomplete="off" disabled="disabled" placeholder="">
            </div>
            <div class="form-group col-md-3">
                <label for="segundoapellido">Segundo apellido:</label>
                <input type="text" class="form-control" id="segundoapellido" autocomplete="off" disabled="disabled" placeholder="">
            </div>

        </div>
        <div class="form-row">

            <div class="form-group col-md-3">
                <label for="numerolicencia">Número Licencia:</label>
                <input type="text" class="form-control" id="numerolicencia"  autocomplete="off" placeholder="">
            </div>
        <div class="form-group col-md-3">
            <label for="fechavencimiento">Fecha vencimiento:</label>
            <input type="text" class="form-control" id="fechavencimiento"  autocomplete="off" placeholder="">
        </div>
        <div class="form-group col-md-3">
            <label for="categoria">Categoría:</label>
            <input type="text" class="form-control" id="categoria"  autocomplete="off" placeholder="">
        </div>

        <div class="form-group col-md-3">
            <label for="restricciones">Restricción:</label>
            <input type="text" class="form-control" id="restricciones"  autocomplete="off" placeholder="">
        </div>

        </div>
    </div>


    <button type="submit" class="btn btn-primary">{{isset($elemento)? 'Actualizar': 'Guardar'}}</button>
@endsection
