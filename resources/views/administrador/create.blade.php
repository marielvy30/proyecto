@extends('layouts.layoutAdministrador')
@section('page')
    Crear Agente
@endsection
@section('content')
    @include('layouts.messages')
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Buscar por cedula
                    </div>
                    <div class="card-body">
                        <form action="{{url('/administrador/create')}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-10">
                                    <input id="cedula" type="text" class="form-control" name='cedula' maxlength="11"
                                           placeholder="Cédula ..."
                                           value="{{isset($usuarioEncon) && $usuarioEncon ? $Agentes->cedula:''}}"/>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);" class="btn btn-success">Buscar...</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="card-body">
            @if(isset($usuarioEncon) && $usuarioEncon )
                <hr>
                <div class="row justify-content-center">
                    <h4>Usuario Encontrado</h4>
                </div>
                <form action="{{url('/guardarAgente')}}" method="post">
                    @csrf

                    @include('layouts.textfield',['obj_id'=>'name',"label_name"=>"Nombres","readonly"=>"","value"=>$Agentes->name])
                    @include('layouts.textfield',["required"=>"",'obj_id'=>'fecha_nacimiento',"label_name"=>"Fecha Nacimiento","readonly"=>"", "value"=>$Agentes->fecha_nacimiento])
                    @include('layouts.textfield',["required"=>"",'obj_id'=>'celular',"label_name"=>"Celular/Teléfono","readonly"=>"",  "minlenght"=>10,"maxlenght"=>10,"value"=>$Agentes->telefono])
                    @include('layouts.textfield',["required"=>"",'obj_id'=>'direccion',"label_name"=>"Dirección","readonly"=>"", "value"=>$Agentes->direccion ])
                    @include('layouts.textfield',['obj_id'=>'email',"label_name"=>"Correo","required"=>"","value"=>""])
                    @include('layouts.textfield',['obj_id'=>'password',"type"=>"password","required"=>"","label_name"=>"Contraseña","value"=>""])
                    @include('layouts.textfield',['obj_id'=>'rango',"label_name"=>"Rango","value"=>isset($Agentes)? $Agentes->rango:""])
                    @include('layouts.textfield',['obj_id'=>'tipo_policia',"label_name"=>"Tipo Policia","value"=>$Agentes->tipo_policia])

                    <div class="form-group row">
                        <div class="col-10">
                            <input class="form-control" type="hidden" id="user_id" name="user_id" required="" value="{{$Agentes->id}}">
                            <input class="form-control" type="hidden" id="tipo" name="tipo" required="" value="2">
                        </div>
                    </div>
                    <button type="submit" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);" class="btn btn-primary btn-block">Guardar</button>
                </form>

            @elseif(isset($usuarioEncon) && !$usuarioEncon)
                <hr>
                <div class="row justify-content-center">
                    <h4>Usuario No Existe</h4>
                </div>
            @endif
        </div>
    </div>
@endsection
