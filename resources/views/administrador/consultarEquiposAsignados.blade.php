@extends('layouts.layoutAdministrador')
@section('page')
    Consulta Equipos Activos
@endsection

@section('content')
    @include('layouts.pagename',['title'=>'Equipos activos','name'=>'Equipos activos'])
    @include('layouts.messages')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Listado Equipos activos
            </div>
            <div class="card-body">

                <div class="table-responsive">

                    <table id="datatable" class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Tablet SIMV</th>
                            <th>Impresora</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($equipos) > 0  )
                            @foreach($equipos as $equipo)
                                <tr>
                                    <td>{{$equipo->policia}}</td>
                                    <td>{{$equipo->nombre}}</td>
                                    <td>{{$equipo->impresora}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra_scripts')
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            } );
        });
    </script>
@endpush
