@extends('layouts.layoutAdministrador')

@section('page')
    Inicio
@endsection

@section('content')
    @include('layouts.pagename',['name'=>'Tablero','title'=>'Tablero'])

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Actualizar
                </div>
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <div class="row">
                        <div class="col-ms-6 col-xlg-3">
                            <div class="card  card-blue">
                                    <a href="/addClientToTxt" class="btn btn-secondary">Actualizar Base de datos</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>

                        <div class="col-ms-6 col-xlg-3">
                            <div class="card card-blue">
                                    <a href="/checkNewUsers" class="btn btn-secondary">Comprobar nuevos Usuarios</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
