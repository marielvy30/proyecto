@extends('layouts.layoutAdministrador')
@section('page')
    Mapa Equipos
@endsection

@section('content')
    @include('layouts.pagename',['title'=>'Equipos activos','name'=>'Equipos activos'])
    @include('layouts.messages')
{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
    <div class="row">
        <div class="col-12">
            <div class="card" >
                <div class="card-header text-info  ">
                    Google Maps
                </div>
                <div class="card-body" >
                    <div id="googleMap"  style="width: 100%; height: 500px" class="map" ></div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function initMap() {
            var place = new google.maps.LatLng(19.45, -70.69);
            var contenido = 'SIMV '
            var conterido2 = '\n Sistema Principal'
            window.map = new google.maps.Map(document.getElementById('googleMap'), {center: place, zoom: 14});

            $.ajax({
                url: "/api/administrador/localidad",
                method: 'get',
                dataType: "json",
            }).done(function(data){
                console.log(data);
                //console.log("data = ", data[0][1]);
                //data = JSON.parse(data);
                $.each(data, function(i, jsonData){
                    //var coords = results.features[i].geometry.coordinates;

                    var latLng = new google.maps.LatLng(data[i]["latitude"],data[i]["longitude"]);
                    var contentString = '<div id="content">'+
                        '<div id="siteNotice">'+
                        '</div>'+
                        '<h5 id="firstHeading" class="firstHeading">SIMV</h5>'+
                        '<div id="bodyContent">'+
                        '<p> Dispositivo '+'</p>'+
                        '</div>'+
                        '</div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });

                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: window.map
                    });

                    marker.addListener('click', function() {
                        infowindow.open(window.map, marker);
                    });
                })
            })
        }
    </script>
    <script>
        setInterval('location.reload()',60000);
       // location.reload(1200000);
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoglLWZxuTrccHb9IMvHCYW5-0Hwz-X4g&libraries=places&callback=initMap"></script>

@endsection
