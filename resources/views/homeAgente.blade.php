@extends('layouts.layoutAgente')

@section('page')
    Agente
@endsection

@section('content')
    @include('layouts.pagename',['name'=>'Inicio','title'=>'Principal'])
    @include('layouts.messages')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Inicio
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-ms-6 col-xlg-3">
                            <div class="card  card-blue">
                                <div class="box text-center" width="200px" height="200px">
                                    <img width="100px" height="100px" src="/img/40216.png" alt="img" />
                                    <a href="/agente/consultarInfraccion" class="btn btn-secondary">Buscar historial de infracción de conductor</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-ms-8 col-xlg-3">
                            <div class="card card-inverse card-blue">
                                <div class="box text-center">
                                    <img width="100px" height="100px" src="/img/antecedentes.png" alt="img" />
                                    <a href="/agente/consultarAntecedentes" class="btn btn-secondary">Buscar Antecedentes de conductor</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-ms-6 col-xlg-3">
                            <div class="card card-inverse card-blue">
                                <div class="box text-center">
                                    <img width="100px" height="100px" src="/img/aplicarmulta.jpg" alt="img" />
                                    <a href="/agente/colocacionMultas" class="btn btn-secondary">Aplicar Infracción </a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
