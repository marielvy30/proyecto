@include('layouts.messages')
<!DOCTYPE HTML>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>DEMO FACTURAS PDF</title>
    <style>
        body {
            font-family: sans-serif, verdana, arial;
        }
        table tr td:first-child
        {
            text-align: right;
        }
    </style>
</head>
<body>
<center>
<img src="./img/digesettLogo.jpg" height="150px" width="400px" align="middle"/>
    </center>
<form>


        <table>
            <tr>
                <td align="left">ID de factura:</td>
                <td><input type="text" name="id_factura" value="{{$pagar->id}}" size="30"></td>

            </tr>
            <tr>
                <td align="left">Fecha emisión de factura:</td>
                <td><input type="text" name="fecha_factura" value="<?php echo date("d/M/Y"); ?>" size="30"></td>
            </tr>
            <tr>
                <td align="left">Hora emisión de factura:</td>
                <td><input type="text" name="Hora_factura" value="<?php echo date("H:i:s"); ?>" size="30"></td>
            </tr>
            <tr>
                <td>Nombre :</td>
                <td><input type="text" name="nombre_tienda" value="SIMV" size="30"></td>
            </tr>
            <tr>
                <td><hr></td>
                <td><hr></td>
            </tr>
            <tr>
                <td>Nombre completo:</td>
                <td><input type="text" name="nombre_cliente" value="{{Auth::user()->name}}" size="30"></td>
            </tr>

            <tr><td>Cédula:</td>
                <td><input type="text" name="direccion_cliente" value="{{Auth::user()->cedula}}" size="30"></td>
            </tr>

        </table>

        <h3>PAGO</h3>

        <table cellpadding="5" border="1">
            <tr><th>Id Multa</th><th>Descripcion</th><th>Monto RD$</th></tr>

                @foreach($facturas as $factura)
                <tr>
                <td>{{$factura->id}}</td>
                <td>
                    @foreach ($factura->multaarticulo as $articulo)
                        {{$factura->descripcion = ""}}
                        {{ $factura->descripcion .= ($articulo->descripcion . " ")}}
                    @endforeach
                </td>
                <td>{{$factura->monto}}</td>
            </tr>
                @endforeach

            <tr>
                <td>Monto pagado RD$:</td>
                <td><input type="text" name="monto" value="" size="50">{{$monto}}</td>

                {{ $qr = $verificacion.-$monto.-$pagar->id.-Auth::user()->cedula }}

                <td><img src="data:img/png;base64, {{ base64_encode(QrCode::format('png')->size(50)->generate($qr))}} "></td>
            </tr>

        </table>

    </form>
    <br><br><br><br>

</body>

</html>
