@extends('layouts.layoutAdministrador')

@section('page')
    Consultar infracciones
@endsection

@section('content')
    @include('layouts.pagename',['name'=>'Consultar infracciones','title'=>'Consultar infracciones de conductor'])
    @include('layouts.messages')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                            <thead>
                            <th>Nombre Conductor</th>
                            <th>Cédula</th>
                                <th>Policia</th>
                                <th>Zona</th>
                                <th>Artículos</th>
                                <th>Estado</th>
                                <th>Fecha</th>
                            </thead>
                            <tbody>
                            @foreach($multas as $multa)
                                <tr>
                                    <td>{{$multa->user->name}}</td>
                                    <td>{{$multa->user->cedula}}</td>
                                    <td>{{$multa->policia->user->name}}</td>
                                    <td>{{$multa->zonaMulta()}}</td>
                                    <td>{{$multa->detalle}}</td>
                                    <td>{{$multa->estado}}</td>
                                    <td>{{$multa->created_at->format('d/M/Y')}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('extra_scripts')
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            } );
        });
    </script>
@endpush
