@extends('layouts.layoutAgente')

@section('page')
    Método de pago
@endsection
@section('content')
    @if(Auth::user()->tipo==2)
        @include('layouts.pagename',['name'=>'Pagar Infracción','title'=>'Pago infracción'])
        @include('layouts.messages')

        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        Infraccion(es) por pagar
                    </div>
                    <div class="card-body">

                        <form action="{{url('/agente/pagarMultas')}}" method="post">
                            @csrf
                            @if(count($multas) > 0)
                                <div class="form-group row">
                                    <label for="nombre"  class="col-2 col-form-label">Nombre dueño tarjeta:</label>
                                    <div class="col-10">
                                        <input id="nombre" type="text" name="nombre" class="form-control" autocomplete="off"  maxlength="20" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tarjeta"   class="col-2 col-form-label">Número tarjeta:</label>
                                    <div class="col-10">
                                        <input id="tarjeta" name="tarjeta" type="text" minlength="16" maxlength="16" onkeypress='return event.charCode>=48 && event.charCode<=57' class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="mes"  name="mest" class="col-2 col-form-label">Mes:</label>
                                    <div class="col-10">
                                        <select id="mes" class="form-control" name="mes">
                                            <option >Seleccione</option>
                                            <option value="01">01 - Enero</option>
                                            <option value="02">02 - Febrero</option>
                                            <option value="03">03 - Marzo</option>
                                            <option value="04">04 - Abril</option>
                                            <option value="05">05 - Mayo</option>
                                            <option value="06">06 - Junio</option>
                                            <option value="07">07 - Julio</option>
                                            <option value="08">08 - Agosto</option>
                                            <option value="09">09 - Septiembre</option>
                                            <option value="10">10 - Octubre</option>
                                            <option value="11">11 - Noviembre</option>
                                            <option value="12">12 - Diciembre</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ano" class="col-2 col-form-label">Año:</label>
                                    <div class="col-10">
                                        <input id="ano" type="text" name="ano" onkeypress='return event.charCode>=48 && event.charCode<=57' class="form-control" maxlength="2" minlength="2">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="cvv" class="col-2 col-form-label">CVV:</label>
                                    <div class="col-10">
                                        <input id="cvv" type="text" name="cvv" onkeypress='return event.charCode>=48 && event.charCode<=57' value="" class="form-control" minlength="3" maxlength="3">
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="datatable" class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                                        <thead>
                                        <th>Infracción</th>
                                        <th>Nombre Conductor</th>
                                        <th>Agente</th>
                                        <th>Zona</th>
                                        <th>Detalle</th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                        <th>monto RD$</th>
                                        <th>¿Cuál(es) desea pagar?<th>
                                        </thead>
                                        <tbody>
                                        @foreach($multas as $multa)
                                            @if($multa->estado == "pendiente" || $multa->estado == "apelada")
                                                <tr>
                                                    <td>{{$multa->id}}</td>
                                                    <td>{{$multa->user->name}}</td>
                                                    <td>{{$multa->policia->user->name}}</td>
                                                    <td>{{$multa->zonaMulta()}}</td>
                                                    <td>{{$multa->descripcion}}</td>
                                                    <td>{{$multa->estado}}</td>
                                                    <td>{{$multa->created_at->format('d/M/Y')}}</td>
                                                    <td>{{$multa->monto}}</td>
                                                    <td width="30%">
                                                        <div class="row ">
                                                            <div class="col-md-6 text-center options">
                                                                <label for="pagar">
                                                                    <input type="checkbox" id="pagar" value="{{$multa->id}}" name="seleccionpagos[]" required="required">
                                                                    Pagar
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @if($pendiente === 0  )
                                    <button type="submit" name="pagar" id="pagar" style="display: none" class="btn btn-primary btn-block">Pagar</button>
                                @elseif($pendiente > 0  )
                                    <button type="submit" name="pagar" id="pagar" class="btn btn-primary btn-block">Pagar</button>
                                @elseif($apelada > 0  )
                                    <button type="submit" name="pagar" id="pagar" class="btn btn-primary btn-block">Pagar</button>
                                @endif
                            @elseif(count($multas) == 0)
                                No tiene multas pendiente por pagar
                                <button type="submit" name="pagar" id="pagar" style="display: none" class="btn btn-primary btn-block">Pagar</button>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>

    @elseif(Auth::user()->tipo==1)
        echo "<script> window.location= '/home' </script>";
    @elseif(Auth::user()->tipo==3)
        echo "<script> window.location= '/homeAdministrador' </script>";
    @endif

@endsection

@push('extra_scripts')
    <script>
        $(function(){
            var requiredCheckboxes = $('.options :checkbox[required]');
            requiredCheckboxes.change(function(){
                if(requiredCheckboxes.is(':checked')) {
                    requiredCheckboxes.removeAttr('required');
                }
                else
                {
                    requiredCheckboxes.attr('required', 'required');
                }
            });
        });
    </script>


    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {


                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "No tiene multas pendientes por pagar",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
        });
    </script>

@endpush
