@extends('layouts.plainLayout')
@section('page')
    Buscar usuario
@endsection
@section('content')
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width initial-scale=1">
    <div class="container" >

        <div class="row justify-content-center">
            <div class="col-md-12" width="150%">
                <div class="card" >

                    <div class="card-header" style="background-color: gray ; color:white ;">
                        <b> Formulario de Registro de Usuario</b>
                    </div>

                    <div class="card-body">

                        <form action="{{url('/buscarUsuario')}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-10" >
                                    <input id="cedula" type="text" autocomplete="off" class="form-control" name='cedula' maxlength="11" onkeypress='return event.charCode>=48 && event.charCode<=57' placeholder="Cédula ..." value="{{isset($usuarioEncontrado) && $usuarioEncontrado ? $usuario->cedula:''}}"/>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);" class="btn btn-success">Buscar...</button>
                                </div>
                            </div>
                        </form>
                        @if(isset($usuarioEncontrado) && $usuarioEncontrado && !$usuario->registrado)
                            <hr>
                            <div class="row justify-content-center">
                                <h4>Usuario Encontrado</h4>
                            </div>
                            <form action="{{url('/guardarUsuario?id='.$usuario->id)}}" method="post">
                                @csrf
                                @include('layouts.textfield',['obj_id'=>'name',"label_name"=>"Nombre Completo","readonly"=>"" , "value"=>$usuario->name])

                                <div class="form-group row">
                                    <label for="fecha" class="col-2 col-form-label">{{ __('Fecha Nacimiento:') }}</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="date" id="fecha" name="fecha" disabled="disabled" required="" value={{$usuario->fecha_nacimiento}}>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="email" class="col-2 col-form-label">{{ __('Correo Electrónico:') }}</label>
                                    <div class="col-md-10">
                                        <input id="email" type="email" autocomplete="off" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" autocomplete="off" class="col-2 col-form-label">{{ __('Contraseña:') }}</label>

                                    <div class="col-md-10">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password-confirm" autocomplete="off" class="col-2 col-form-label">{{ __('Confirmar Contraseña:') }}</label>

                                    <div class="col-md-10">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="celular" autocomplete="off" class="col-2 col-form-label">Teléfono:</label>
                                    <div class="col-10">
                                        <input class="form-control"  autocomplete="off" type="text" id="celular" maxlength="10" minlength="10" name="celular" required="" value="" onkeypress='return event.charCode>=48 && event.charCode<=57'>
                                    </div>
                                </div>
                                @include('layouts.textfield',["required"=>"",'obj_id'=>'direccion',"label_name"=>"Dirección", "value"=>$usuario->direccion ])

                                <div class="form-group row">
                                    <div class="col-10">
                                        <input class="form-control" type="hidden" id="registrado" name="registrado" required="" value="1">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-10">
                                        <input class="form-control" type="hidden" id="tipo" name="tipo" required="" value="1">
                                    </div>
                                </div>
                                <button type="submit" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);" class="btn btn-primary btn-block">Guardar</button>
                            </form>
                        @elseif(isset($usuarioEncontrado) && $usuarioEncontrado && $usuario->registrado)
                            <hr>
                            <div class="row justify-content-center">
                                <h4>Usuario Ya Registrado</h4>
                            </div>
                        @elseif(isset($usuarioEncontrado) && !$usuarioEncontrado)
                            <hr>
                            <div class="row justify-content-center">
                                <h4>Usuario No Encontrado</h4>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
