@extends('layouts.layoutAgente')
@section('page')
    Acta de Infracción
@endsection

@section('content')
    @include('layouts.pagename',['name'=>'Aplicar Infracción','title'=>'Acta de infracción'])
    @include('layouts.messages')
    @inject('marcas', 'App\Services\Marcas')

    <link rel="stylesheet" href="css/colocacionMultas.css">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Búsqueda de conductor.
                </div>

                <div class="card-body">

                    @include('layouts.selectSiNo',["label_name"=>"licencia","label"=>"¿El conductor tiene licencia?"])

                    <form action="{{url('/agente/colocacionMultas')}}" method="post" name="miform" id="miform" style="display: none"  >
                        @csrf
                        <div class="form-group row">
                            <input id="cedula" class="form-control" name='cedula' maxlength="11" type="hidden"
                               value="{{isset($usuarioEncontra) && $usuarioEncontra ? $usuario->cedula:''}}"/>
                            <label  class="col-2 col-form-label">Lectura licencia:</label>
                            <div class="col-10">
                                <input id="cedulaView" type="text" autocomplete="off" disabled="disabled" class="form-control" name='cedulaView'
                                   placeholder="Cédula ..." maxlength="11" value="{{isset($usuarioEncontra) && $usuarioEncontra ? $usuario->cedula:''}}"/>
                            </div>
                        </div>
                        <p id="demo" type="hidden"></p>
                    </form>

                    <form action="{{url('/agente/colocacionMultas')}}" method="post" name="miformSinLicencia" id="miformSinLicencia"   style="display: none" >

                        @csrf
                        <div class="form-group row">
                            <input id="cedulaQR" class="form-control" name='cedulaQR' maxlength="36" type="hidden"
                                   value="{{isset($usuarioEncontra) && $usuarioEncontra ? $usuario->qr:''}}"/>
                            <label  class="col-2 col-form-label">Lectura QR de la cédula:</label>
                            <div class="col-10">
                                <input id="cedulaViewQR" type="text" autocomplete="off" disabled="disabled" class="form-control" style="text-transform: uppercase" name='cedulaViewQR'
                                       placeholder="Código ..." maxlength="36" value="{{isset($usuarioEncontra) && $usuarioEncontra ? $usuario->qr:''}}"/>
                            </div>
                        </div>
                        <p id="demo" type="hidden"></p>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <form action="{{url('/agente/guardarMulta')}}"  name="guardaMulta" id="guardaMulta" method="post">
                <!--Si el usuario que se lee en la licencia esta registrado-->
                @if(isset($usuarioEncontra) && $usuarioEncontra && $usuario->registrado && $usuario->estatus=='Activo')

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    Datos del conductor
                                </div>
                                <div class="card-body">
                                <hr>
                                <div class="form-group row">
                                    <div class="col-10">
                                        <center> <pre><img class="card-img-top" src="/foto/{{$usuario->foto_perfil}}" style="max-width: 200px; max-height: 250px"  alt="" align="center" ></pre>
                                        </center>
                                        <center> <label  class="col-2 col-form-label">Firma:</label><p><img class="card-img-top" src="/firma/{{$usuario->foto_firma}}" style="max-width: 200px; max-height: 250px"alt="" align="middle">
                                            </p></center>
                                    </div>
                                </div>

                                @csrf
                                @include('layouts.textfields',["required"=>"",'obj_id'=>'name',"label_name"=>"Nombre completo","readonly"=>"" , "value"=>$usuario->name])
                                @include('layouts.textfields',["required"=>"",'obj_id'=>'estatus_cedula',"label_name"=>'Estatus',"readonly"=>"" , "value"=>$usuario->estatus])
                                @include('layouts.textfields',["required"=>"",'obj_id'=>'fecha_nacimiento',"label_name"=>'Fecha Nacimiento',"readonly"=>"" , "value"=>$usuario->fecha_nacimiento])
                                @include('layouts.textfields',["required"=>"",'obj_id'=>'celular',"label_name"=>'Teléfono',"readonly"=>"" , "value"=>$usuario->telefono])
                                @include('layouts.textfields',["required"=>"",'obj_id'=>'email',"label_name"=>'Correo',"readonly"=>"" , "value"=>$usuario->email])

                                    <div class="form-group">
                                        <input type="text" style="display:none" required="required" name="mobile" class="form-control" id="mobile" value="{{$usuario->telefono}}" placeholder="Teléfono/celular">
                                    </div>

                                </div>
                                </div>
                        </div>
                    </div>

            <!-- aqui empieza la consulta antes de aplicar la multa-->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            @if(isset($usuarioEncon) && $usuarioEncon && count($elementos)>0)
                                Infracciones de {{$usuario->name}}
                            @elseif(isset($usuarioEncon) && $usuarioEncon && !count($elementos)>0)
                                No hay Infracciones para este conductor: {{$usuario->name}}
                            @else
                                Antecedentes del conductor
                            @endif

                        </div>
                        <div class="card-body">
                            {{--          <button id="aplicar" class="btn btn-success" onclick="datosmulta()">Aplicar multa</button>--}}

                            <div class="table-responsive">
                                <table id="datatable" name="tabla_multa"  class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                                    <thead>
                                    <tr>
                                        <th>Tipo (Tránsito/Delictiva)</th>
                                        <th>Detalle de Antecedentes  </th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                    </tr>
                                             </thead>
                                    <tbody>

                                    @foreach($multas as $multa)
                                        @if ($multa->user->cedula == $usuario->cedula)
                                            <tr>
                                                <td>Tránsito</td>
                                                <td>{{$multa->descripcion}}</td>
                                                <td>{{$multa->estado}}</td>
                                                <td>{{$multa->created_at->format('d/M/Y')}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    @foreach($elementos as $elemento)
                                        @if ($multa->user->cedula == $usuario->cedula)
                                            <tr>
                                                <td>Delictiva</td>
                                                <td>{{$elemento->descripcion}}</td>
                                                <td>N/A</td>
                                                <td>{{$elemento->created_at->format('d/M/Y')}}</td>
                                            </tr>
                                        @endif
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--aqui termina la consulta-->



            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Datos del vehículo
                        </div>
                        <div class="card-body">

                            <div class="form-group row">
                                <label for="tipo_vehiculo" class="col-2 col-form-label">Tipo Vehículo: </label>
                                <div class="col-10">
                                    <select class="select2 custom-select" required="" id="tipo_vehiculo" name="tipo_vehiculo">
                                        <option value="">Seleccione</option>
                                        <option value="Autobus">Autobús</option>
                                        <option value="Automovil">Automóvil</option>
                                        <option value="Camioneta">Camioneta</option>
                                        <option value="Motocicleta">Motocicleta</option>
                                        <option value="Otro">Otro</option>
                                    </select>
                                </div>
                            </div>
                            <!--select de marca-->
                            <div class="form-group row">
                                <label for="marca" style='' name="marcad" class="col-2 col-form-label">Marca</label>
                                <div class="col-10">
                                    <select id="marca" name="marca_id" style='' class="form-control{{ $errors->has('marca_id') ? ' is-invalid' : '' }}">
                                        @foreach($marcas->get() as $index => $marca)
                                            <option value="{{ $index }}" {{ old('marca_id') == $index ? 'selected' : '' }}>
                                                {{ $marca }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--Select de modelo-->
                            <div class="form-group row">
                                <label for="modelo" style='' name="modelod" class="col-2 col-form-label">Modelo</label>
                                <div class="col-10">
                                    <select id="modelo" data-old="{{ old('modelo_id') }}" style='' name="modelo_id" class="form-control{{ $errors->has('modelo_id') ? ' is-invalid' : '' }}"></select>
                                    @if ($errors->has('modelo_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('modelo_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="placa" class="col-2 col-form-label"> Placa :</label>
                                <div class="col-10">
                                    <input class="form-control" maxlength="7" minlength="7" autocomplete="off" id="placa" name="placa" required="" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="descripcion" class="col-2 col-form-label"> Observación :</label>
                                <div class="col-10">
                                    <input class="form-control" maxlength="45" autocomplete="off" type="text" id="descripcion" name="descripcion" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="direccion" class="col-2 col-form-label"> Dirección :</label>
                                <div class="col-10">
                                    <input class="form-control" maxlength="45" autocomplete="off" type="text" id="direccion" name="direccion" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-10">
                                    <input id="user_id" type="hidden" class="form-control" name="user_id" maxlength="11" value="{{$usuario->id}}">
                                    <input id="policia_id" type="hidden" class="form-control" name="policia" value="{{ Auth::guard('policia')->user()->id }}">
                                    <input type="datetime" name="fecha" style="display:none" id="fecha"  value="<?php echo date("Y-m-d");?>">
                                    <input type="datetime" name="hora" style="display:none" id="hora" value="<?php echo date("H-i A");?>">
                                    <input type="input" name="longitud" style="display:none" id="longitud"  value="{{$coordena->longitude}}">
                                    <input type="input" name="latitud" style="display:none" id="latitud" value="{{$coordena->latitude}}">


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Listado de infracciones
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">

                                <!-- Aqui esta la tabla de infraccion-->

                                <table name="tabla_infraccion"  style="display: block; width:100%; text-align:center; vertical-align:middle; height: 600px" class="display mt-5  nowrap table table-hover table-striped table-bordered options " cellspacing="0" width="95%">

                                    <thead>
                                    <tr>
                                        <th scope="col">Seleccione</th>
                                        <th scope="col">Descripción</th>
                                        <th scope="col">Seleccione</th>
                                        <th scope="col">Descripción</th>
                                        <th scope="col">Seleccione</th>
                                        <th scope="col">Descripción</th>
                                        <th scope="col">Seleccione</th>
                                        <th scope="col">Descripción</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa42"><input type="checkbox" value="42" id="multa42" name="articulos[]" required="required">
                                                        Art-42
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            Tablilla de identificación personal.
                                        </td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa143"><input type="checkbox" required="required" value="143" id="multa143" name="articulos[]">
                                                        Art-143
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Conservación de señales de tránsito.</td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-18"><input type="checkbox" value="189-18" required="required" id="multa189-18" name="articulos[]">
                                                        Art-189 No.18
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Modificar color consignado en la matrícula.</td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa233"><input type="checkbox" required="required" value="233" id="multa233" name="articulos[]">
                                                        Art-233
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Precaución al encontrarse con un autobús escolar.</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa44"><input type="checkbox" style="display: none" id="multa44h" value="44" name="articulos[]">
                                                    </label>
                                                    <label for="multa44"><input type="checkbox" disabled="disabled" id="multa44" value="44" name="articulos[]">
                                                        Art-44
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Operación sin licencia o rótulo de identidad autorizado.</td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa144"><input type="checkbox" required="required" id="multa144" value="144" name="articulos[]">
                                                        Art-144
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Conservación de las vías públicas y paseos.</td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-19"><input type="checkbox" required="required" id="multa189-19" value="189-19" name="articulos[]">
                                                        Art-189 No.19
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Tirar o empujar un vehículo por otro vehiculo de motor no diseñado para tales fines.
                                        </td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa235"><input type="checkbox" required="required" id="multa235" value="235" name="articulos[]">
                                                        Art-235
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Giro prohibido.</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa62"><input type="checkbox" required="required" value="62" id="multa62" name="articulos[]">
                                                        Art-62
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Transporte de carga o mercancías en vehículos destinados al transporte público de
                                            pasajero.
                                        </td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa145"><input type="checkbox" required="required" id="multa145" value="145" name="articulos[]">
                                                        Art-145
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Uso de las vías públicas y paseos.</td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa191"><input type="checkbox" required="required" id="multa191" value="191" name="articulos[]">
                                                        Art-191
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Emisiones contaminantes y conversión de vehículos de motor.</td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa236"><input type="checkbox" required="required" id="multa236" value="236" name="articulos[]">
                                                        Art-236
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Señales de los conductores.</td>
                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa63"><input type="checkbox" required="required" id="multa63" value="63" name="articulos[]">
                                                        Art-63
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Uso de animales como medio de tracción en las vías públicas.</td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa146"><input type="checkbox" required="required" id="multa146" value="146" name="articulos[]">
                                                        Art-146
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Obstáculos al libre tránsito.</td>

                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa192"><input type="checkbox" required="required" id="multa192" value="192" name="articulos[]">
                                                        Art-192
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Requisitos de equipamiento.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa237"><input type="checkbox" required="required" id="multa237" value="237" name="articulos[]">
                                                        Art-237
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Estacionamiento en lugar prohibido.</td>

                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa64-1"><input type="checkbox" required="required" id="multa64-1" value="64-1" name="articulos[]">
                                                        Art-64 No.1
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Luces de motocicletas.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa147"><input type="checkbox" required="required" id="multa147" value="147" name="articulos[]">
                                                        Art-147
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Puestos de ventas en las vías públicas.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa193"><input type="checkbox" required="required" id="multa193" value="193" name="articulos[]">
                                                        Art-193
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Obstrucciones a la visibilidad del conductor.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa238"><input type="checkbox" required="required" id="multa238" value="238" name="articulos[]">                     Art-238
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Parada en las intersecciones.</td>

                                    </tr>
                                    <tr>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa64-2"><input type="checkbox" required="required" id="multa64-2" value="64-2" name="articulos[]">
                                                        Art-64 No.2
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Franjas de material reflectivo.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa155"><input type="checkbox" id="multa155" required="required" value="155" name="articulos[]">
                                                        Art-155
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Cruce sobre mangueras de bomberos.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa194"><input type="checkbox" required="required" id="multa194" value="194" name="articulos[]">
                                                        Art-194
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Aparatos receptores de imágenes.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa239"><input type="checkbox" required="required" id="multa239" value="239" name="articulos[]">
                                                        Art-239
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Uso del freno de emergencia.</td>

                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa64-3"><input type="checkbox" required="required" id="multa64-3" value="64-3" name="articulos[]">
                                                        Art-64 No.3
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Luces que indiquen largo total del vehículo.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa156"><input type="checkbox" id="multa156" required="required" value="156" name="articulos[]">
                                                        Art-156
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Las ciclovías.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa195"><input type="checkbox" id="multa195" required="required" value="195" name="articulos[]">
                                                        Art-195
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Parachoques.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa241"><input type="checkbox" required="required" id="multa241" value="241" name="articulos[]">
                                                        Art-241
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Inicio de la marcha.</td>

                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa67"><input type="checkbox" required="required" id="multa67" value="67" name="articulos[]">
                                                        Art-67
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Número de pasajeros.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa158"><input type="checkbox" required="required" id="multa158" value="158" name="articulos[]">
                                                        Art-158
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Colocación de propagandas en vías públicas.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa210"><input type="checkbox" required="required" id="multa210" value="210" name="articulos[]">
                                                        Art-210
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>No porte de licencia de conducir o licencia vencida.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa246"><input type="checkbox" required="required" id="multa246" value="246" name="articulos[]">
                                                        Art-246
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Transitar por la derecha.</td>

                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa68"><input type="checkbox" required="required" id="multa68" value="68" name="articulos[]">
                                                        Art-68
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Capacidad de asientos.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa167"><input type="checkbox" required="required" id="multa167" value="167" name="articulos[]">
                                                        Art-167
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Marbete de Inspección Técnica Vehicular.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa217"><input type="checkbox" required="required" id="multa217" value="217" name="articulos[]">
                                                        Art-217
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>No porte de seguro o seguro vencido.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa247"><input type="checkbox" required="required" id="multa247" value="247" name="articulos[]">
                                                        Art-247
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Excepciones para el tránsito por la derecha.</td>

                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa69"><input type="checkbox" required="required" id="multa69" value="69" name="articulos[]">
                                                        Art-69
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Transporte de niños menores de 12 años en asiento trasero; de 6-12 con cinturón de seguridad y menos de 6 años en asiento especial para infantes.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa170"><input type="checkbox" required="required" id="multa170" value="170" name="articulos[]">
                                                        Art-170
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Perdida de condiciones de seguridad.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa218-219"><input type="checkbox" required="required" id="multa218-219" value="218-219" name="articulos[]">
                                                        Art-218 y 219
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Circulación a los peatones.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa248"><input type="checkbox" required="required" id="multa248" value="248" name="articulos[]">
                                                        Art-248
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Alcanzar y pasar por la izquierda.</td>

                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa78"><input type="checkbox" required="required" id="multa78" value="78" name="articulos[]">
                                                        Art-78
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Prohibición de niños menores de 8 años en motocicletas y de más de dos pasajeros.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa174"><input type="checkbox" required="required" id="multa174" value="174" name="articulos[]">
                                                        Art-174
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Modificación de vehículos de motor o remolques.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa220"><input type="checkbox" required="required" id="multa220" value="220" name="articulos[]">
                                                        Art-220
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Conducción temeraria.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa249"><input type="checkbox" required="required" id="multa249" value="249" name="articulos[]">
                                                        Art-249
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Deber del conductor alcanzado.</td>

                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa96"><input type="checkbox" required="required" id="multa96" value="96" name="articulos[]">
                                                        Art-96
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Vehículos para transpote funerario.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-1"><input type="checkbox" required="required" id="multa189-1" value="189-1" name="articulos[]">
                                                        Art-189 No.1
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Sin matrícula.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa221"><input type="checkbox" required="required" id="multa221" value="221" name="articulos[]">
                                                        Art-221
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Distracción durante la conducción.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa250"><input type="checkbox" required="required" id="multa250" value="250" name="articulos[]">
                                                        Art-250
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Conducción entre carriles.</td>

                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa98"><input type="checkbox" required="required" id="multa98" value="98" name="articulos[]">
                                                        Art-98
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Prohibición para transportar pasajeros en vehículos fúnebres.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-2"><input type="checkbox" required="required" id="multa189-2" value="189-2" name="articulos[]">
                                                        Art-189 No.2
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Conducir un vehículo o tirar de un remolque en un uso distinto al autorizado por la matrícula.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa222"><input type="checkbox" required="required" id="multa222" value="222" name="articulos[]">
                                                        Art-222</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Deberes de los conductores hacia los peatones.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa251"><input type="checkbox" required="required" id="multa251" value="251" name="articulos[]">
                                                        Art-251
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>No uso de casco protector homologado, chalecos, o aditamentos de ropas reflectantes. Transitar en prupos de
                                            dos o más en paralelo, sujetarse de otro vehículo en movimiento, transitar por túneles, pasos a desnivel y en vía contraria</td>
                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa120"><input type="checkbox" required="required" id="multa120" value="120" name="articulos[]">
                                                        Art-120
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Zonas y horarios de circulación de transporte de carga.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-4"><input type="checkbox" required="required" id="multa189-4" value="189-4" name="articulos[]">
                                                        Art-189 No.4
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Sin placa.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa223"><input type="checkbox" required="required" id="multa223" value="223" name="articulos[]">
                                                        Art-223
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Precaución al encontrarse con un autobús escolar.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa252"><input type="checkbox" required="required" id="multa252" value="252" name="articulos[]">
                                                        Art-252
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Cruzarse en sentidos opuestos.</td>

                                    </tr>
                                    <tr>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa122"><input type="checkbox" required="required" id="multa122" value="122" name="articulos[]">
                                                        Art-122
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Prohibición a la circulación de transporte de carga con sobrepeso.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-6"><input type="checkbox" required="required" id="multa189-6" value="189-6" name="articulos[]">
                                                        Art-189 No.6
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Borrar o alterar información en el certificado de propiedad (matrícula).</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa224"><input type="checkbox" required="required" id="multa224" value="224" name="articulos[]">
                                                        Art-224
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Distancia a mantener entre vehículos.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa253"><input type="checkbox" required="required" id="multa253" value="253" name="articulos[]">
                                                        Art-253
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Movimiento en retroceso.</td>

                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa124"><input type="checkbox" required="required" id="multa124" value="124" name="articulos[]">
                                                        Art-124
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Prohibición del transporte de pasajeros sobre la carga.</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-7"><input type="checkbox" required="required" id="multa189-7" value="189-7" name="articulos[]">
                                                        Art-189 No.7
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Placas mutiladas, alteradas, fotocopiadas, etc.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa225"><input type="checkbox" required="required" id="multa225" value="225" name="articulos[]">
                                                        Art-225
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Precaución al encontrarse con animales.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa254"><input type="checkbox" required="required" id="multa254" value="254" name="articulos[]">
                                                        Art-254
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Ceder el paso.</td>
                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa133"><input type="checkbox" required="required" id="multa133" value="133" name="articulos[]">
                                                        Art-133
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Respeto a las señales del Semáforo.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-8"><input type="checkbox" required="required" id="multa189-8" value="189-8" name="articulos[]">
                                                        Art-189 No.8</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Aditamentos a la placa.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa226"><input type="checkbox" required="required" id="multa226" value="226" name="articulos[]">
                                                        Art-226
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Responsabilidad del propietariode animales.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa256"><input type="checkbox" required="required" id="multa256" value="256" name="articulos[]">
                                                        Art-256
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Conducir en estado de embriaguez.</td>
                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa135"><input type="checkbox" required="required" id="multa135" value="135" name="articulos[]">
                                                        Art-135
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Semáforo para los peatones.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-9"><input type="checkbox" required="required" id="multa189-9" value="189-9" name="articulos[]">
                                                        Art-189 No.9
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Uso de documentos falsos o alterados.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa227-228-229"><input type="checkbox" required="required" id="multa227-228-229" value="227-228-229" name="articulos[]">
                                                        Art-227, 228 y 229
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Uso de putos, sirenas, bocinas, luces giratorias, intermitentes o rojas.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa257"><input type="checkbox" required="required" id="multa257" value="257" name="articulos[]">
                                                        Art-257
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Conducción bajo los efectos de drogas o sustancias controladas.</td>
                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa137"><input type="checkbox" required="required" id="multa137" value="137" name="articulos[]">
                                                        Art-137
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Semáforo de carriles.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-11"><input type="checkbox"  required="required" id="multa189-11" value="189-11" name="articulos[]">
                                                        Art-189 No.11
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Alterar o modificar la lectura del odómetro.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa230"><input type="checkbox" required="required" id="multa230" value="230" name="articulos[]">
                                                        Art-230
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Deslizamiento en neutro por cuestas.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa264"><input type="checkbox" required="required" id="multa264" value="264" name="articulos[]">
                                                        Art-264
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Límites de velocidad.</td>
                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa139"><input type="checkbox" required="required" id="multa139" value="139" name="articulos[]">
                                                        Art-139
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Semáforo de tránsito ante cruces ferroviarios.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-13"><input type="checkbox" required="required" id="multa189-13" value="189-13" name="articulos[]">
                                                        Art-189 No.13
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Sin marbete o placa vencida.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa231-1"><input type="checkbox" required="required" id="multa231-1" value="231-1" name="articulos[]">
                                                        Art-231 No.1
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Uso de putos, sirenas, bocinas, luces giratorias, intermitentes o rojas.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa266"><input type="checkbox" required="required" id="multa266" value="266" name="articulos[]">
                                                        Art-266
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Lugares de velocidad regulada.</td>
                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa140"><input type="checkbox" required="required" id="multa140" value="140" name="articulos[]">
                                                        Art-140
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Señales de tránsito ante intersecciones.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-14"><input type="checkbox" required="required" id="multa189-14" value="189-14" name="articulos[]">
                                                        Art-189 No.14
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Placas no previstas por la ley.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa231-2"><input type="checkbox" required="required" id="multa231-2" value="231-2" name="articulos[]">
                                                        Art-231 No.2
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Guía a la derecha.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa267"><input type="checkbox" required="required" id="multa267" value="267" name="articulos[]">
                                                        Art-267
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Competencia de velocidad en las vías públicas.</td>
                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa141"><input type="checkbox" required="required" id="multa141" value="141" name="articulos[]">
                                                        Art-141
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Marca en el pavimento, bordillo y en el contén.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-15"><input type="checkbox" required="required" id="multa189-15" value="189-15" name="articulos[]">
                                                        Art-189 No.15
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Indicadores de peso y capacidad en vehículo de carga.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa231-4"><input type="checkbox" required="required" id="multa231-4" value="231-4" name="articulos[]">
                                                        Art-231 No.4
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Lanzar desperdios a la vía pública.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa268"><input type="checkbox" required="required" id="multa268" value="268" name="articulos[]">
                                                        Art-268
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Límites máximos de velocidad.</td>
                                    </tr>
                                    <tr>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa142"><input type="checkbox" required="required" id="multa142" value="142" name="articulos[]">
                                                        Art-142
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Señales y marcas no autorizadas.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa189-17"><input type="checkbox" required="required" id="multa189-17" value="189-17" name="articulos[]">
                                                        Art-189 No.17
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Placa vencida, suspendida o cancelada.</td>
                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa231-5"><input type="checkbox" required="required" id="multa231-5" value="231-5" name="articulos[]">
                                                        Art-231 No.5
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>No uso del cinturón de seguridad</td>

                                        <td><div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label for="multa269"><input type="checkbox" required="required" id="multa269" value="269" name="articulos[]">
                                                        Art-269
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Velocidad muy reducida.</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!--Aqui termina la tabla-->

                            </div>
                            <br>
                            <button type="submit" style="background-color: rgb(25, 101, 185);" class="btn btn-primary btn-block">Aplicar infracción</button>
                            </br>
                        </div>
                    </div>
                </div>
            </div>

        @elseif(isset($usuarioEncontra) && $usuarioEncontra && $usuario->registrado && $usuario->estatus=='Cancelada')

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Datos del conductor
                        </div>
                        <div class="card-body">
                            <hr>
                            <div class="form-group row">
                                <div class="col-10">
                                    <center> <pre><img class="card-img-top" src="/foto/{{$usuario->foto_perfil}}" style="max-width: 200px; max-height: 250px"  alt="" align="center" ></pre>
                                    </center>
                                </div>
                            </div>
                            @csrf
                            @include('layouts.textfields',["required"=>"",'obj_id'=>'name',"label_name"=>"Nombre completo","readonly"=>"" , "value"=>$usuario->name])
                            @include('layouts.textfields',["required"=>"",'obj_id'=>'estatus_cedula',"label_name"=>'Estatus',"readonly"=>"" , "value"=>$usuario->estatus])
                            @include('layouts.textfields',["required"=>"",'obj_id'=>'fecha_nacimiento',"label_name"=>'Fecha Nacimiento',"readonly"=>"" , "value"=>$usuario->fecha_nacimiento])

                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row justify-content-center">
                <h4>Usuario registrado ha sido cancelado o inhabilitado</h4>
            </div>
        @elseif(isset($usuarioEncontra) && $usuarioEncontra && !$usuario->registrado && $usuario->estatus=='Cancelada')

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Datos del conductor
                        </div>
                        <div class="card-body">
                            <hr>
                            <div class="form-group row">
                                <div class="col-10">
                                    <center> <pre><img class="card-img-top" src="/foto/{{$usuario->foto_perfil}}" style="max-width: 200px; max-height: 250px"  alt="" align="center" ></pre>
                                    </center>
                                </div>
                            </div>

                            @csrf
                            @include('layouts.textfields',["required"=>"",'obj_id'=>'name',"label_name"=>"Nombre completo","readonly"=>"" , "value"=>$usuario->name])
                            @include('layouts.textfields',["required"=>"",'obj_id'=>'estatus_cedula',"label_name"=>'Estatus',"readonly"=>"" , "value"=>$usuario->estatus])
                            @include('layouts.textfields',["required"=>"",'obj_id'=>'fecha_nacimiento',"label_name"=>'Fecha Nacimiento',"readonly"=>"" , "value"=>$usuario->fecha_nacimiento])

                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row justify-content-center">
                <h4>Usuario registrado ha sido cancelado o inhabilitado</h4>
            </div>

            <!--Si el usuario que se lee en la licencia no esta registrado-->
        @elseif(isset($usuarioEncontra) && $usuarioEncontra && !$usuario->registrado && $usuario->estatus=='Activo')

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                Datos del conductor
                            </div>
                            <div class="card-body">
                                <hr>
                                <div class="form-group row">
                                    <div class="col-10">
                                        <center> <pre><img class="card-img-top" src="/foto/{{$usuario->foto_perfil}}" style="max-width: 200px; max-height: 250px"  alt="" align="center" ></pre>
                                        </center>
                                        <center> <label  class="col-2 col-form-label">Firma:</label><p><img class="card-img-top" src="/firma/{{$usuario->foto_firma}}" style="max-width: 200px; max-height: 250px"alt="" align="middle">
                                            </p></center>
                                    </div>
                                </div>

                                @csrf
                                @include('layouts.textfields',["required"=>"",'obj_id'=>'name',"label_name"=>"Nombre completo","readonly"=>"" , "value"=>$usuario->name])
                                @include('layouts.textfields',["required"=>"",'obj_id'=>'estatus_cedula',"label_name"=>'Estatus',"readonly"=>"" , "value"=>$usuario->estatus])
                                @include('layouts.textfields',["required"=>"",'obj_id'=>'fecha_nacimiento',"label_name"=>'Fecha Nacimiento',"readonly"=>"" , "value"=>$usuario->fecha_nacimiento])

                                <div class="form-group row">
                                    <label for="mobile" class="col-2 col-form-label"> Teléfono :</label>
                                    <div class="col-10">
                                        <input type="text"  required="required" minlength="10" maxlength="10" name="mobile" class="form-control" id="mobile" value="" placeholder="Mobile number">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                        <!-- aqui empieza la consulta antes de aplicar la multa-->
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @if(isset($usuarioEncon) && $usuarioEncon && count($elementos)>0)
                                            Infracciones de {{$usuario->name}}
                                        @elseif(isset($usuarioEncon) && $usuarioEncon && !count($elementos)>0)
                                            No hay Infracciones para este conductor: {{$usuario->name}}
                                        @else
                                            Antecedentes del conductor
                                        @endif

                                    </div>
                                    <div class="card-body">

                                        <div class="table-responsive">
                                            <table id="datatable" name="tabla_multa"  class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                                                <thead>
                                                <tr>
                                                    <th>Tipo (Tránsito/Delictiva)</th>
                                                    <th>Detalle de Antecedentes  </th>
                                                    <th>Estado</th>
                                                    <th>Fecha</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($multas as $multa)
                                                    @if ($multa->user->cedula == $usuario->cedula)
                                                        <tr>
                                                            <td>Tránsito</td>
                                                            <td>{{$multa->descripcion}}</td>
                                                            <td>{{$multa->estado}}</td>
                                                            <td>{{$multa->created_at->format('d/M/Y')}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                @foreach($elementos as $elemento)
                                                    @if ($multa->user->cedula == $usuario->cedula)
                                                        <tr>
                                                            <td>Delictiva</td>
                                                            <td>{{$elemento->descripcion}}</td>
                                                            <td>N/A</td>
                                                            <td>{{$elemento->created_at->format('d/M/Y')}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--aqui termina la consulta-->


                        <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    Datos del vehículo
                                </div>
                                <div class="card-body">

                                <div class="form-group row">
                                    <label for="tipo_vehiculo" class="col-2 col-form-label">Tipo Vehículo: </label>
                                    <div class="col-10">
                                        <select class="select2 custom-select" required="" id="tipo_vehiculo" name="tipo_vehiculo">
                                            <option value="">Seleccione</option>
                                            <option value="Autobus">Autobús</option>
                                            <option value="Automovil">Automóvil</option>
                                            <option value="Camioneta">Camioneta</option>
                                            <option value="Motocicleta">Motocicleta</option>
                                            <option value="Otro">Otro</option>
                                        </select>
                                    </div>
                                </div>
                                <!--select de marca-->
                                <div class="form-group row">
                                    <label for="marca" style='' name="marcad" class="col-2 col-form-label">Marca</label>
                                    <div class="col-10">
                                        <select id="marca" name="marca_id" style='' class="form-control{{ $errors->has('marca_id') ? ' is-invalid' : '' }}">
                                            @foreach($marcas->get() as $index => $marca)
                                                <option value="{{ $index }}" {{ old('marca_id') == $index ? 'selected' : '' }}>
                                                    {{ $marca }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!--Select de modelo-->
                                <div class="form-group row">
                                    <label for="modelo" style='' name="modelod" class="col-2 col-form-label">Modelo</label>
                                    <div class="col-10">
                                        <select id="modelo" data-old="{{ old('modelo_id') }}" style='' name="modelo_id" class="form-control{{ $errors->has('modelo_id') ? ' is-invalid' : '' }}"></select>
                                        @if ($errors->has('modelo_id'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('modelo_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="placa" class="col-2 col-form-label"> Placa :</label>
                                    <div class="col-10">
                                        <input class="form-control" maxlength="7" minlength="7" autocomplete="off" id="placa" name="placa" required="" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="descripcion" class="col-2 col-form-label"> Observación :</label>
                                    <div class="col-10">
                                        <input class="form-control" autocomplete="off" type="text" id="descripcion"  maxlength="45" name="descripcion" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="direccion" class="col-2 col-form-label"> Dirección :</label>
                                    <div class="col-10">
                                        <input class="form-control" autocomplete="off" type="text" id="direccion" maxlength="45" name="direccion" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-10">
                                        <input id="user_id" type="hidden" class="form-control" name="user_id" maxlength="11" value="{{$usuario->id}}">
                                        <input id="policia_id" type="hidden" class="form-control" name="policia" value="{{ Auth::guard('policia')->user()->id }}">
                                        <input type="datetime" style="display:none" name="fecha" id="fecha"  value="<?php echo date("d-m-Y");?>">
                                        <input type="datetime"  style="display:none" name="hora" id="hora" value="<?php echo date("H-i A");?>">
                                        <input type="input" name="longitud" style="display:none" id="longitud"  value="{{$coordena->longitude}}">
                                        <input type="input" name="latitud" style="display:none" id="latitud" value="{{$coordena->latitude}}">

                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    Listado de infracciones
                                </div>
                                <div class="card-body">

                                <div class="table-responsive">
                                    <!-- Aqui esta la tabla de infraccion-->

                                    <table name="tabla_infraccion"  style="display: block; width:100%; text-align:center; vertical-align:middle; height: 600px" class="display mt-5  nowrap table table-hover table-striped table-bordered options" cellspacing="0" width="95%">

                                        <thead>
                                        <tr>
                                            <th scope="col">Seleccione</th>
                                            <th scope="col">Descripción</th>
                                            <th scope="col">Seleccione</th>
                                            <th scope="col">Descripción</th>
                                            <th scope="col">Seleccione</th>
                                            <th scope="col">Descripción</th>
                                            <th scope="col">Seleccione</th>
                                            <th scope="col">Descripción</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa42"><input type="checkbox" value="42" id="multa42" name="articulos[]" required="required">
                                                            Art-42
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                Tablilla de identificación personal.
                                            </td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa143"><input type="checkbox" required="required" value="143" id="multa143" name="articulos[]">
                                                            Art-143
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Conservación de señales de tránsito.</td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-18"><input type="checkbox" value="189-18" required="required" id="multa189-18" name="articulos[]">
                                                            Art-189 No.18
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Modificar color consignado en la matrícula.</td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa233"><input type="checkbox" required="required" value="233" id="multa233" name="articulos[]">
                                                            Art-233
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Precaución al encontrarse con un autobús escolar.</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa44"><input type="checkbox" required="required" style="display: none" id="multa44h" value="44" name="articulos[]">
                                                        </label>
                                                        <label for="multa44"><input type="checkbox" required="required" disabled="disabled" id="multa44" value="44" name="articulos[]">
                                                            Art-44
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Operación sin licencia o rótulo de identidad autorizado.</td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa144"><input type="checkbox" required="required" id="multa144" value="144" name="articulos[]">
                                                            Art-144
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Conservación de las vías públicas y paseos.</td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-19"><input type="checkbox" required="required" id="multa189-19" value="189-19" name="articulos[]">
                                                            Art-189 No.19
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Tirar o empujar un vehículo por otro vehiculo de motor no diseñado para tales fines.
                                            </td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa235"><input type="checkbox" required="required" id="multa235" value="235" name="articulos[]">
                                                            Art-235
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Giro prohibido.</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa62"><input type="checkbox" required="required" value="62" id="multa62" name="articulos[]">
                                                            Art-62
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Transporte de carga o mercancías en vehículos destinados al transporte público de
                                                pasajero.
                                            </td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa145"><input type="checkbox" required="required" id="multa145" value="145" name="articulos[]">
                                                            Art-145
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Uso de las vías públicas y paseos.</td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa191"><input type="checkbox" required="required" id="multa191" value="191" name="articulos[]">
                                                            Art-191
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Emisiones contaminantes y conversión de vehículos de motor.</td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa236"><input type="checkbox" required="required" id="multa236" value="236" name="articulos[]">
                                                            Art-236
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Señales de los conductores.</td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa63"><input type="checkbox" required="required" id="multa63" value="63" name="articulos[]">
                                                            Art-63
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Uso de animales como medio de tracción en las vías públicas.</td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa146"><input type="checkbox" required="required" id="multa146" value="146" name="articulos[]">
                                                            Art-146
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Obstáculos al libre tránsito.</td>

                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa192"><input type="checkbox" required="required" id="multa192" value="192" name="articulos[]">
                                                            Art-192
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Requisitos de equipamiento.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa237"><input type="checkbox" required="required" id="multa237" value="237" name="articulos[]">
                                                            Art-237
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Estacionamiento en lugar prohibido.</td>

                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa64-1"><input type="checkbox" required="required" id="multa64-1" value="64-1" name="articulos[]">
                                                            Art-64 No.1
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Luces de motocicletas.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa147"><input type="checkbox" required="required" id="multa147" value="147" name="articulos[]">
                                                            Art-147
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Puestos de ventas en las vías públicas.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa193"><input type="checkbox" required="required" id="multa193" value="193" name="articulos[]">
                                                            Art-193
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Obstrucciones a la visibilidad del conductor.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa238"><input type="checkbox" required="required" id="multa238" value="238" name="articulos[]">                     Art-238
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Parada en las intersecciones.</td>

                                        </tr>
                                        <tr>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa64-2"><input type="checkbox" required="required" id="multa64-2" value="64-2" name="articulos[]">
                                                            Art-64 No.2
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Franjas de material reflectivo.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa155"><input type="checkbox" id="multa155" required="required" value="155" name="articulos[]">
                                                            Art-155
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Cruce sobre mangueras de bomberos.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa194"><input type="checkbox" required="required" id="multa194" value="194" name="articulos[]">
                                                            Art-194
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Aparatos receptores de imágenes.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa239"><input type="checkbox" required="required" id="multa239" value="239" name="articulos[]">
                                                            Art-239
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Uso del freno de emergencia.</td>

                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa64-3"><input type="checkbox" required="required" id="multa64-3" value="64-3" name="articulos[]">
                                                            Art-64 No.3
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Luces que indiquen largo total del vehículo.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa156"><input type="checkbox" id="multa156" required="required" value="156" name="articulos[]">
                                                            Art-156
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Las ciclovías.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa195"><input type="checkbox" id="multa195" required="required" value="195" name="articulos[]">
                                                            Art-195
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Parachoques.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa241"><input type="checkbox" required="required" id="multa241" value="241" name="articulos[]">
                                                            Art-241
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Inicio de la marcha.</td>

                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa67"><input type="checkbox" required="required" id="multa67" value="67" name="articulos[]">
                                                            Art-67
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Número de pasajeros.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa158"><input type="checkbox" required="required" id="multa158" value="158" name="articulos[]">
                                                            Art-158
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Colocación de propagandas en vías públicas.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa210"><input type="checkbox" required="required" id="multa210" value="210" name="articulos[]">
                                                            Art-210
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>No porte de licencia de conducir o licencia vencida.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa246"><input type="checkbox" required="required" id="multa246" value="246" name="articulos[]">
                                                            Art-246
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Transitar por la derecha.</td>

                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa68"><input type="checkbox" required="required" id="multa68" value="68" name="articulos[]">
                                                            Art-68
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Capacidad de asientos.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa167"><input type="checkbox" required="required" id="multa167" value="167" name="articulos[]">
                                                            Art-167
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Marbete de Inspección Técnica Vehicular.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa217"><input type="checkbox" required="required" id="multa217" value="217" name="articulos[]">
                                                            Art-217
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>No porte de seguro o seguro vencido.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa247"><input type="checkbox" required="required" id="multa247" value="247" name="articulos[]">
                                                            Art-247
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Excepciones para el tránsito por la derecha.</td>

                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa69"><input type="checkbox" required="required" id="multa69" value="69" name="articulos[]">
                                                            Art-69
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Transporte de niños menores de 12 años en asiento trasero; de 6-12 con cinturón de seguridad y menos de 6 años en asiento especial para infantes.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa170"><input type="checkbox" required="required" id="multa170" value="170" name="articulos[]">
                                                            Art-170
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Perdida de condiciones de seguridad.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa218-219"><input type="checkbox" required="required" id="multa218-219" value="218-219" name="articulos[]">
                                                            Art-218 y 219
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Circulación a los peatones.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa248"><input type="checkbox" required="required" id="multa248" value="248" name="articulos[]">
                                                            Art-248
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Alcanzar y pasar por la izquierda.</td>

                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa78"><input type="checkbox" required="required" id="multa78" value="78" name="articulos[]">
                                                            Art-78
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Prohibición de niños menores de 8 años en motocicletas y de más de dos pasajeros.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa174"><input type="checkbox" required="required" id="multa174" value="174" name="articulos[]">
                                                            Art-174
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Modificación de vehículos de motor o remolques.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa220"><input type="checkbox" required="required" id="multa220" value="220" name="articulos[]">
                                                            Art-220
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Conducción temeraria.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa249"><input type="checkbox" required="required" id="multa249" value="249" name="articulos[]">
                                                            Art-249
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Deber del conductor alcanzado.</td>

                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa96"><input type="checkbox" required="required" id="multa96" value="96" name="articulos[]">
                                                            Art-96
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Vehículos para transpote funerario.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-1"><input type="checkbox" required="required" id="multa189-1" value="189-1" name="articulos[]">
                                                            Art-189 No.1
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Sin matrícula.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa221"><input type="checkbox" required="required" id="multa221" value="221" name="articulos[]">
                                                            Art-221
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Distracción durante la conducción.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa250"><input type="checkbox" required="required" id="multa250" value="250" name="articulos[]">
                                                            Art-250
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Conducción entre carriles.</td>

                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa98"><input type="checkbox" required="required" id="multa98" value="98" name="articulos[]">
                                                            Art-98
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Prohibición para transportar pasajeros en vehículos fúnebres.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-2"><input type="checkbox" required="required" id="multa189-2" value="189-2" name="articulos[]">
                                                            Art-189 No.2
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Conducir un vehículo o tirar de un remolque en un uso distinto al autorizado por la matrícula.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa222"><input type="checkbox" required="required" id="multa222" value="222" name="articulos[]">
                                                            Art-222</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Deberes de los conductores hacia los peatones.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa251"><input type="checkbox" required="required" id="multa251" value="251" name="articulos[]">
                                                            Art-251
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>No uso de casco protector homologado, chalecos, o aditamentos de ropas reflectantes. Transitar en prupos de
                                                dos o más en paralelo, sujetarse de otro vehículo en movimiento, transitar por túneles, pasos a desnivel y en vía contraria</td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa120"><input type="checkbox" required="required" id="multa120" value="120" name="articulos[]">
                                                            Art-120
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Zonas y horarios de circulación de transporte de carga.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-4"><input type="checkbox" required="required" id="multa189-4" value="189-4" name="articulos[]">
                                                            Art-189 No.4
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Sin placa.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa223"><input type="checkbox" required="required" id="multa223" value="223" name="articulos[]">
                                                            Art-223
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Precaución al encontrarse con un autobús escolar.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa252"><input type="checkbox" required="required" id="multa252" value="252" name="articulos[]">
                                                            Art-252
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Cruzarse en sentidos opuestos.</td>

                                        </tr>
                                        <tr>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa122"><input type="checkbox" required="required" id="multa122" value="122" name="articulos[]">
                                                            Art-122
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Prohibición a la circulación de transporte de carga con sobrepeso.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-6"><input type="checkbox" required="required" id="multa189-6" value="189-6" name="articulos[]">
                                                            Art-189 No.6
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Borrar o alterar información en el certificado de propiedad (matrícula).</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa224"><input type="checkbox" required="required" id="multa224" value="224" name="articulos[]">
                                                            Art-224
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Distancia a mantener entre vehículos.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa253"><input type="checkbox" required="required" id="multa253" value="253" name="articulos[]">
                                                            Art-253
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Movimiento en retroceso.</td>

                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa124"><input type="checkbox" required="required" id="multa124" value="124" name="articulos[]">
                                                            Art-124
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Prohibición del transporte de pasajeros sobre la carga.</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-7"><input type="checkbox" required="required" id="multa189-7" value="189-7" name="articulos[]">
                                                            Art-189 No.7
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Placas mutiladas, alteradas, fotocopiadas, etc.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa225"><input type="checkbox" required="required" id="multa225" value="225" name="articulos[]">
                                                            Art-225
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Precaución al encontrarse con animales.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa254"><input type="checkbox" required="required" id="multa254" value="254" name="articulos[]">
                                                            Art-254
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Ceder el paso.</td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa133"><input type="checkbox" required="required" id="multa133" value="133" name="articulos[]">
                                                            Art-133
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Respeto a las señales del Semáforo.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-8"><input type="checkbox" required="required" id="multa189-8" value="189-8" name="articulos[]">
                                                            Art-189 No.8</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Aditamentos a la placa.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa226"><input type="checkbox" required="required" id="multa226" value="226" name="articulos[]">
                                                            Art-226
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Responsabilidad del propietariode animales.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa256"><input type="checkbox" required="required" id="multa256" value="256" name="articulos[]">
                                                            Art-256
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Conducir en estado de embriaguez.</td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa135"><input type="checkbox" required="required" id="multa135" value="135" name="articulos[]">
                                                            Art-135
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Semáforo para los peatones.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-9"><input type="checkbox" required="required" id="multa189-9" value="189-9" name="articulos[]">
                                                            Art-189 No.9
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Uso de documentos falsos o alterados.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa227-228-229"><input type="checkbox" required="required" id="multa227-228-229" value="227-228-229" name="articulos[]">
                                                            Art-227, 228 y 229
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Uso de putos, sirenas, bocinas, luces giratorias, intermitentes o rojas.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa257"><input type="checkbox" required="required" id="multa257" value="257" name="articulos[]">
                                                            Art-257
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Conducción bajo los efectos de drogas o sustancias controladas.</td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa137"><input type="checkbox" required="required" id="multa137" value="137" name="articulos[]">
                                                            Art-137
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Semáforo de carriles.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-11"><input type="checkbox"  required="required" id="multa189-11" value="189-11" name="articulos[]">
                                                            Art-189 No.11
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Alterar o modificar la lectura del odómetro.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa230"><input type="checkbox" required="required" id="multa230" value="230" name="articulos[]">
                                                            Art-230
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Deslizamiento en neutro por cuestas.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa264"><input type="checkbox" required="required" id="multa264" value="264" name="articulos[]">
                                                            Art-264
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Límites de velocidad.</td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa139"><input type="checkbox" required="required" id="multa139" value="139" name="articulos[]">
                                                            Art-139
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Semáforo de tránsito ante cruces ferroviarios.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-13"><input type="checkbox" required="required" id="multa189-13" value="189-13" name="articulos[]">
                                                            Art-189 No.13
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Sin marbete o placa vencida.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa231-1"><input type="checkbox" required="required" id="multa231-1" value="231-1" name="articulos[]">
                                                            Art-231 No.1
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Uso de putos, sirenas, bocinas, luces giratorias, intermitentes o rojas.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa266"><input type="checkbox" required="required" id="multa266" value="266" name="articulos[]">
                                                            Art-266
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Lugares de velocidad regulada.</td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa140"><input type="checkbox" required="required" id="multa140" value="140" name="articulos[]">
                                                            Art-140
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Señales de tránsito ante intersecciones.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-14"><input type="checkbox" required="required" id="multa189-14" value="189-14" name="articulos[]">
                                                            Art-189 No.14
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Placas no previstas por la ley.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa231-2"><input type="checkbox" required="required" id="multa231-2" value="231-2" name="articulos[]">
                                                            Art-231 No.2
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Guía a la derecha.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa267"><input type="checkbox" required="required" id="multa267" value="267" name="articulos[]">
                                                            Art-267
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Competencia de velocidad en las vías públicas.</td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa141"><input type="checkbox" required="required" id="multa141" value="141" name="articulos[]">
                                                            Art-141
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Marca en el pavimento, bordillo y en el contén.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-15"><input type="checkbox" required="required" id="multa189-15" value="189-15" name="articulos[]">
                                                            Art-189 No.15
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Indicadores de peso y capacidad en vehículo de carga.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa231-4"><input type="checkbox" required="required" id="multa231-4" value="231-4" name="articulos[]">
                                                            Art-231 No.4
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Lanzar desperdios a la vía pública.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa268"><input type="checkbox" required="required" id="multa268" value="268" name="articulos[]">
                                                            Art-268
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Límites máximos de velocidad.</td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa142"><input type="checkbox" required="required" id="multa142" value="142" name="articulos[]">
                                                            Art-142
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Señales y marcas no autorizadas.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa189-17"><input type="checkbox" required="required" id="multa189-17" value="189-17" name="articulos[]">
                                                            Art-189 No.17
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Placa vencida, suspendida o cancelada.</td>
                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa231-5"><input type="checkbox" required="required" id="multa231-5" value="231-5" name="articulos[]">
                                                            Art-231 No.5
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>No uso del cinturón de seguridad</td>

                                            <td><div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label for="multa269"><input type="checkbox" required="required" id="multa269" value="269" name="articulos[]">
                                                            Art-269
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Velocidad muy reducida.</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!--Aqui termina la tabla-->
                                </div>
                                    <br>
                                <button type="submit" style="background-color: rgb(25, 101, 185);" class="btn btn-primary btn-block">Aplicar infracción</button>
                                    </br>
                                </div>
                            </div>
                        </div>
                    </div>

        @elseif(isset($usuarioEncontra) && !$usuarioEncontra && !$usuario->registrado)
            <hr>
            <div class="row justify-content-center">
                <h4>Usuario No Existente</h4>
            </div>
        @endif
    </form>

@endsection


@push('extra_scripts')

    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {


                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "El conductor registrado no tiene infracciones",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
        });
    </script>
@endpush


@section('scripts')

    <script>
        $(document).ready(function(){
            $('#marca').on('change', function(){
                var marca_id= $(this).val();
                if ($.trim(marca_id) != ''){
                    $.get('/modelos', {marca_id: marca_id}, function (modelos){
                        $('#modelo').empty();
                        $('#modelo').append("<option value=''> Seleccione modelo</option>");
                        $.each(modelos, function(index,value){
                            $('#modelo').append("<option value='"+ index +"'> "+ value +"</option>");
                        })
                    });
                }
            });
        });
    </script>

    <script>
//cuando recargue la pagina se queda el valor del select
        window.onload = function() {
            var selItem = sessionStorage.getItem("SelItem");
            $('#licencia').val(selItem);
            pregunta();

        }
        $('#licencia').change(function() {
            var selVal = $(this).val();
            sessionStorage.setItem("SelItem", selVal);
            pregunta();
        });
    </script>

    <script>
        function pregunta()
        {
            if(document.getElementById('licencia').value === "Si")
            {
                document.getElementById('miform').style.display ="inline";
                document.getElementById('miformSinLicencia').style.display ="none";

                var presses = [];
                var cedula = " ";

                window.addEventListener("keydown", function (evt)
                {
                    if (evt.key !== "Enter")
                    {
                        presses.push(evt.key);
                    }
                    let sumSymbol = 0;

                    presses.forEach(letter =>
                    {
                        if (letter === "|")
                        {
                            sumSymbol++;
                        }
                    if (letter === "]")
                    {
                        sumSymbol++;
                    }
                    });

                    if (sumSymbol === 4)
                    {
                        ready();
                    }
                });
                function ready()
                {
                    cedula = presses.splice(0, 11).join(''), 10;
                    $(document).ready(function ()
                    {
                        $("#cedula").val(cedula);
                        $("#cedulaView").val(cedula);
                        cargar();
                    });
                    clearKeylogger();
                }
                function cargar()
                {
                    document.forms["miform"].submit();
                }
                function clearKeylogger()
                {
                    presses = [];
                }
            }

            if(document.getElementById('licencia').value === "No")
            {
                document.getElementById('miform').style.display ="none";
                document.getElementById('miformSinLicencia').style.display ="inline";

                $("#multa44").prop('checked', true);
                $("#multa44h").prop('checked', true);

                var pressesqr = [];
                var cedulaqr = " ";

                window.addEventListener("keypress", function (evt)
                {
                    if (evt.key !== "Enter")
                    {
                    pressesqr.push(evt.key);
                    }

                    let sumSymbol = 0;
                    let sumCaracter = 0;

                    pressesqr.forEach(letterqr =>
                    {
                        if (letterqr === "-")
                        {
                            sumSymbol++;
                        }

                        sumCaracter++;
                    });
                    if (sumSymbol === 4 && sumCaracter === 36)
                    {
                        readyqr();
                    }
                });
                function readyqr()
                {
                    cedulaqr = pressesqr.splice(0, 36).join(''), 35;
                    $(document).ready(function ()
                    {
                        $("#cedulaQR").val(cedulaqr);
                        $("#cedulaViewQR").val(cedulaqr);
                        cargarqr();
                    });
                    clearKeylogger();
                }
                function cargarqr()
                {
                    document.forms["miformSinLicencia"].submit();
                }
                function clearKeylogger()
                {
                    pressesqr = [];
                }
            }
        }
    </script>

    <script>
        //script deshabilitar el select de la licecnia cuando se cargue los datos

        var licenci = document.getElementById('licencia');
        if(document.getElementById('cedulaQR').value.length === 36 || document.getElementById('cedula').value.length === 11)
        {
            $("#licencia").attr("Disabled","Disabled");
        }
        else
        {
            licenci.disabled = false;
        }
    </script>

    <script>

        $(function(){
            var requiredCheckboxes = $('.options :checkbox[required]');
            requiredCheckboxes.change(function(){
                if(requiredCheckboxes.is(':checked')) {
                    requiredCheckboxes.removeAttr('required');
                } else {
                    requiredCheckboxes.attr('required', 'required');
                }
            });
        });
    </script>

@endsection
