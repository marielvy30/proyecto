@extends('layouts.layoutAgente')
@section('page')
    Listado de infracción
@endsection

@section('content')
    @include('layouts.pagename',['title'=>"Infracciones",'name'=>"Infracciones"])
    @include('layouts.messages')
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Buscar por cedula
                    </div>
                    <div class="card-body">
                        <form>
                            @csrf
                                <div class="row">
                                    <div class="col-md-10">
                                        <input id="cedula" type="text" class="form-control" autocomplete="off" name='cedula' maxlength="11"
                                               placeholder="Cédula ..."
                                               value="{{isset($usuarioEncon) && $usuarioEncon ? $usuario->cedula:''}}"/>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" style="background-color: rgb(25, 101, 185);border: rgb(25, 101, 185);" class="btn btn-success">Buscar...</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @if(isset($usuarioEncon) && $usuarioEncon && count($elementos)>0)
                        Infracciones de {{$usuario->name}}
                    @elseif(isset($usuarioEncon) && $usuarioEncon && !count($elementos)>0)
                        No hay Infracciones para este conductor: {{$usuario->name}}
                    @else
                        Infracciones
                    @endif

                </div>
                <div class="card-body">
                    <div class="table-responsive">

                        <table id="datatable" class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Descripcion</th>
                                <th>Estado</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($elementos) > 0  )
                                @foreach($elementos as $elemento)
                                    <tr>
                                        <td>{{$elemento->created_at->format('d/M/Y')}}</td>
                                        <td>{{$elemento->descripcion}}</td>
                                        <td>{{$elemento->estado}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('extra_scripts')
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "El conductor buscado no tiene infracciones",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
        });
    </script>
@endpush
