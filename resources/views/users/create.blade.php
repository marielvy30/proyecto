@extends('layouts.layoutAdministrador')
@section('content')
    <div class="card">
            <form action="{{url('/users/create')}}">
                @csrf
                <div class="row">
                    <div class="col-md-10">
                        <input id="cedula" type="text" class="form-control" name='cedula'  maxlength="11" placeholder="Cédula ..." value="{{isset($usuarioEncon) && $usuarioEncon ? $Agentes->cedula:''}}"/>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success">Buscar...</button>
                    </div>
                </div>
            </form>
            @if(isset($usuarioEncon) && $usuarioEncon )
            @if ( $Agentes->tipo =='1')
            <hr>
                <div class="row justify-content-center">
                    <h4>Usuario Encontrado</h4>
                </div>
            <div class="card-body">
                @include('include.errorAlert')
                <form action="{{url('/guardarAgente?id='.$Agentes->id)}}" method="post">
                    @csrf

                    <!--datos para la tabla de usuario-->
                    @include('layouts.textfield',['obj_id'=>'name',"label_name"=>"Nombres","readonly"=>"","value"=>$Agentes->name])
                    @include('layouts.textfield',['obj_id'=>'email',"label_name"=>"Correo","value"=>$Agentes->email])
                    @include('layouts.textfield',['obj_id'=>'password',"type"=>"password","required"=>"","label_name"=>"Contraseña", "value"=>$Agentes->password ])
                    @include('layouts.textfield',["required"=>"",'obj_id'=>'fecha_nacimiento',"label_name"=>"Fecha Nacimiento", "value"=>$Agentes->fecha_nacimiento])
                    @include('layouts.selectSexo',['label_name'=>'sexo',"label"=>"Sexo", "value"=>$Agentes->sexo ])
                    @include('layouts.textfield',["required"=>"",'obj_id'=>'celular',"label_name"=>"Celular/Teléfono",  "minlenght"=>10,"maxlenght"=>10,"value"=>$Agentes->celular])
                    @include('layouts.textfield',["required"=>"",'obj_id'=>'direccion',"label_name"=>"Dirección", "value"=>$Agentes->direccion ])
                    <!--datos para la tabla de policia-->
                        @include('layouts.textfield',['obj_id'=>'rango',"label_name"=>"Rango","value"=>isset($Agentes)? $Agentes->rango:""])
                        @include('layouts.textfield',['obj_id'=>'tipo_policia',"label_name"=>"Tipo Policia","value"=>$Agentes->tipo_policia])

                        <input id="user_id" type="hidden"  class="form-control" name='user_id' maxlength="11" value="{{$Agentes->id}}"/>
                        <hr>
                    <div class="form-group row">
                        <div class="col-10">
                            <input class="form-control" type="hidden" id="registrado" name="registrado" required="" value="1">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-10">
                            <input class="form-control" type="hidden" id="tipo" name="tipo" required="" value="2">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Guardar</button>

                </form>
                @elseif($Agentes->tipo =='2')
                    <hr>
                    <div class="row justify-content-center">
                        <h4>Usuario Registrado</h4>
                    </div>
                @endif
                @elseif(isset($usuarioEncon) && !$usuarioEncon)
                    <hr>
                    <div class="row justify-content-center">
                        <h4>Usuario No Existe</h4>
                    </div>
                @endif
            </div>
    </div>
@endsection
