@extends('layouts.layoutAdministrador')

@section('page')
    Inicio
@endsection

@section('content')
    @include('layouts.pagename',['name'=>'Tablero','title'=>'Tablero'])

    <div class="row">
        <div class="col-md-6 col-lg-3 col-xlg-3">
            @include('layouts.db_info',['color'=>'info','value'=>isset($count_users)?$count_users :0,'info'=>'Usuarios'])
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Inicio
                </div>
                <div class="card-body">
                    <h5 class="card-title">Principales</h5>
                    <a href="/users/create" class="btn btn-secondary">Añadir Agente </a>
                </div>
            </div>
        </div>
    </div>

@endsection
