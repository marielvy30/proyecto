@extends('layouts.layoutConductor')

@section('page')
    Inicio
@endsection

@section('content')

    @include('layouts.pagename',['name'=>'Tablero','title'=>'Tablero'])
    @include('layouts.messages')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Inicio
                </div>
                <div class="card-body">
                    <h5 class="card-title">Principales</h5>
                    <div class="row">
                        <div class="col-ms-6 col-xlg-3">
                            <div class="card  card-blue">
                                <div class="box text-center" width="200px" height="200px">
                                    <img width="100px" height="100px" src="/img/40216.png" alt="img" />
                                    <a href="/conductor/consultaXconductor" class="btn btn-secondary">Consultar Infracción</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-ms-8 col-xlg-3">
                            <div class="card card-inverse card-blue">
                                <div class="box text-center">
                                    <img width="100px" height="100px" src="/img/pagon.png" alt="img" />
                                    <a href="/conductor/pagoMultas" class="btn btn-secondary">Pagar Infracción</a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-ms-6 col-xlg-3">
                            <div class="card card-inverse card-blue">
                                <div class="box text-center">
                                    <img width="100px" height="100px" src="/img/cancelarmulta.png" alt="img" />
                                    <a href="/conductor/ImpugnarMulta" class="btn btn-secondary">Impugnar Infracción </a>
                                    <h1 class="font-light text-white"></h1>
                                    <h6 class="text-white"></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
