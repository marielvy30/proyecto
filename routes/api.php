<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/sendGps', function (Request $request) {
    if ($request->serial) {
        $equipo = \App\Equipo::where('serial', $request->serial)->first();
        if ($equipo == null) {
            $equipo = new \App\Equipo();
            $equipo->serial = $request->serial;
            $equipo->nombre = $request->serial;
            $equipo->estado = "Activo";
            $equipo->save();
        }
        $newGps = new \App\Gps();
        $newGps->latitude = $request->latitude;
        $newGps->longitude = $request->longitude;
        $newGps->equipo_id = $equipo->id;
        $newGps->save();

        return ["success" => true,"gps"=>$newGps];
    }

    return ["success" => false];

});
Route::get("/administrador/localidad", function (){
    $query = DB::select(DB::raw("select tt.equipo_id, tt.latitude, tt.longitude from gps tt inner join (select equipo_id, max(id) as max from gps group by equipo_id) groupedtt ON tt.equipo_id= groupedtt.equipo_id and tt.id = max;"));
    return $query;
});

Route::get('/checkNewUsers', 'GuestController@checkClients');
Route::get('/addClientToTxt', 'GuestController@addClientToTxt');
