<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Input;
use App\User;

Route::view('/', 'welcome');

Auth::routes();
//Logins
Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm')->name('login.admin');
Route::get('/login/policia', 'Auth\LoginController@showPoliciaLoginForm')->name('login.policia');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/login/policia', 'Auth\LoginController@policiaLogin');

Route::get('/home', 'HomeController@index')->name('home');
//Route::view('/home', 'homeConductor')->middleware('auth');
Route::resource('users', 'UserController');

////////vistas para que el conductor se pueda registrar/////
Route::post("/guardarUsuario", 'GuestController@guardarUsuario');
Route::get("/buscarUsuario", 'GuestController@buscarUsuario');
Route::get('/register/verify/{code}', 'GuestController@verify');

////////////////////rutas del conductor/////////////////
Route::group(['middleware' => 'auth'], function () {
//consulta de las infracciones que tiene el conductor
    Route::get("/conductor/consultaXconductor", 'UserController@consultaXconductor');
//pago de la multa
    Route::get("/conductor/pagoMultas", 'UserController@pago');
    Route::post("/conductor/pagoMultas", 'UserController@pago', 'UserController@pagando');
    Route::post("/conductor/pagarMultas", 'UserController@pagando', 'UserController@pago');
    Route::get("/conductor/pagarMultas", 'UserController@pagos');

//apelacion
    Route::get("/conductor/ApelarMultas", 'UserController@apelar', 'UserController@apelacion');
    Route::post("/conductor/ApelarMultas", 'UserController@apelar', 'UserController@apelacion');
    Route::get("/conductor/ImpugnarMulta", 'UserController@vistaApelar');
});

////////////////////rutas del administrador/////////////////
///
Route::group(['middleware' => 'auth:admin'], function () {
    Route::view('/homeAdministrador', 'homeAdministrador');

//    Route::get('/loginAdmin', 'Auth\LoginAdminController@showLoginForm')->name('home');
//    Route::post('/loginAdmin', ['as' => 'loginAdmin', 'uses' => 'Auth\LoginAdminController@login']);
//    Route::get('/logoutAdmin', 'Auth\LoginAdminController@logout');
//    Route::post('/logoutAdmin', 'Auth\LoginAdminController@logout');

//agregar nuevos equipos

    Route::get("/administrador/crearEquipos", 'AdminController@CrearEquipos');
    Route::post("/guardarEquipo", 'AdminController@guardarEquipo');
    Route::get("/guardarEquipo", 'AdminController@CrearEquipos');
    Route::get("/administrador/localidad", 'AdminController@localidad');


///
    Route::get("/administrador/listado", 'AdminController@listado');
    Route::get("/administrador/create", 'AdminController@createAgente');
    Route::post("/guardarAgente", 'AdminController@guardarAgente');

    Route::get("/administrador/agregarUsuario", 'AdminController@usuarios');
    Route::get('/checkNewUsers', 'GuestController@checkClients');
    Route::get('/addClientToTxt', 'GuestController@addClientToTxt');

    Route::get("/administrador/consultarEquipos", 'AdminController@equipos');
    Route::get("/administrador/EquiposAlmacen", 'AdminController@equiposAlmacen');
    Route::get("/administrador/AsignarEquipos", 'AdminController@AsignarEquipos');
    Route::post("/AsignacionEquipos", 'AdminController@AsignacionEquipos');
    Route::get("/administrador/mapa", 'AdminController@mapa');
    Route::get("/administrador/consultarEquiposAsignados", 'AdminController@consultaEquiposActivos');
});


////////////rutas del agente/////////////////////////
Route::group(['middleware' => 'auth:policia'], function () {
    Route::view('/homeAgente', 'homeAgente');
    //consulta conductor por multas

    Route::get("/agente/consultarInfraccion", 'PoliciaController@consultarInfraccion');

//consulta conductor por antecedentes

    Route::get("/agente/consultarAntecedentes", 'PoliciaController@consultarAntecedentes');

//guarda la multa enviando correo de notificacion

    Route::post("/agente/guardarMulta", 'MultaController@guardarMulta');
    Route::post("/agente/guardarMultas", 'MultaController@guardarMultas');

//carga los datos del conductor

    Route::get("/agente/colocacionMultas", 'MultaController@colocacionMultas');
    Route::get("/administrador/localidad", 'AdminController@localidad');
    Route::post("/agente/colocacionMultas", 'MultaController@colocacionMultas');

//carga los datos de los modelos segun la marca

    Route::get('/modelos', 'MultaController@GetModelos');

});

//Route::get("",'');
//Route::post("",'');
//////////////por organizar
//Route::get('/administrador/registrarLicencia', 'LicenciaController@index');
//Route::get("/imprimir", 'ModeloController@index');
//Route::get("/agente/consultaAgente", 'UserController@index');
//    Route::get('/loginAgente', 'Auth\LoginAgenteController@showLoginForm')->name('home');
//    Route::post('/loginAgente', ['as' => 'loginAgente', 'uses' => 'Auth\LoginAgenteController@login']);
//    Route::get('/logoutAgente', 'Auth\LoginAgenteController@logout');
//    Route::post('/logoutAgente', 'Auth\LoginAgenteController@logout');
