<?php

use Illuminate\Database\Seeder;

class ArticuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('articulos')->insert([
            [
                'numero_articulo' => "42",
                'descripcion' => "Tablilla de identificación personal.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "44",
                'descripcion' => "Operación sin licencia o rótulo de identidad autorizado.",
                'monto' => 1500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "62",
                'descripcion' => "Transporte de carga o mercancías en vehículos destinados al transporte público de pasajero.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "63",
                'descripcion' => "Uso de animales como medio de tracción en las vías públicas.",
                'monto' => 1500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "64-1",
                'descripcion' => "Luces de motocicletas.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "64-2",
                'descripcion' => "Franjas de material reflectivo.",
                'monto' => 2500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "64-3",
                'descripcion' => "Luces que indiquen largo total del vehículo.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "67",
                'descripcion' => "Número de pasajeros.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "68",
                'descripcion' => "Capacidad de asientos.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "69",
                'descripcion' => "Transporte de niños de 12 años en asiento trasero; de 6-12 años en asiento especial para infantes.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "78",
                'descripcion' => "Prohibición de niños menores de 8 años en motocicletas y de más de dos pasajeros.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],

            [
                'numero_articulo' => "96",
                'descripcion' => "Vehículos para el transporte funerario.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "98",
                'descripcion' => "Prohibición a la circulación de transporte de carga con sobrepeso.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],            [
                'numero_articulo' => "120",
                'descripcion' => "Zonas y horarios de circulación de transporte de carga.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "122",
                'descripcion' => "Prohibición a la circulación de transporte con sobrepeso.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "124",
                'descripcion' => "Prohibición del transporte de pasajeros sobre la carga.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "133",
                'descripcion' => "Respeto a las señales del Semáforo.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "135",
                'descripcion' => "Semáforo para peatones.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "137",
                'descripcion' => "Semáforo de carriles.",
                'monto' => 1500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "139",
                'descripcion' => "Señales de tránsito ante cruces ferroviarios.",
                'monto' => 1500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "140",
                'descripcion' => "Señales de tránsito ante intersecciones.",
                'monto' => 1500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "141",
                'descripcion' => "Marcas en el pavimento, bordillo y en el contén.",
                'monto' => 1500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],

            [
                'numero_articulo' => "142",
                'descripcion' => "Señales y marcas no autorizadas.",
                'monto' => 5000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "143",
                'descripcion' => "Conservación de las señales de tránsito.",
                'monto' => 5000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "144",
                'descripcion' => "Conservación de las vías públicas y paseos.",
                'monto' => 5000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "145",
                'descripcion' => "Uso de las vías públicas y paseos.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "146",
                'descripcion' => "Obstáculos al libre tránsito.",
                'monto' => 5000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "147",
                'descripcion' => "Puestos de ventas en las vías públicas.",
                'monto' => 3000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "155",
                'descripcion' => "Cruce sobre mangueras de bomberos.",
                'monto' => 3000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "156",
                'descripcion' => "Las ciclovías.",
                'monto' => 3000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "158",
                'descripcion' => "Colocación de propaganda en las vías públicas.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "167",
                'descripcion' => "Marbete de Inspección Técnica Vehicular.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "170",
                'descripcion' => "Pérdida de condiciones de seguridad.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "174",
                'descripcion' => "Modificación de vehículos de motor o remolques.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "189-1",
                'descripcion' => "Sin matrícula.",
                'monto' => 5000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "189-2",
                'descripcion' => "Conducir un vehículo o tirar de un remolque en un uso distinto al autorizado por la matrícula.",
                'monto' => 5000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "189-4",
                'descripcion' => "Sin placa.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "189-6",
                'descripcion' => "Borrar o alterar información en el certificado de propiedad(matrícula).",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "189-7",
                'descripcion' => "Placas mutiladas, alteradas, fotocopiadas, etc.",
                'monto' => 3500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],

            [
                'numero_articulo' => "189-8",
                'descripcion' => "Adimentos en la placa.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],

            [
                'numero_articulo' => "189-9",
                'descripcion' => "Uso de documentos falsos o alterados.",
                'monto' => 2500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],

            [
                'numero_articulo' => "189-11",
                'descripcion' => "Alterar o modificar la lectura del odómetro.",
                'monto' => 1700,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],

            [
                'numero_articulo' => "189-13",
                'descripcion' => "Sin marbete o placa vencida.",
                'monto' => 1700,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],

            [
                'numero_articulo' => "189-14",
                'descripcion' => "Placas no previstas por la ley.",
                'monto' => 1800,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "189-15",
                'descripcion' => "Indicadores de peso y capacidad en vehículo de carga.",
                'monto' => 1900,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "189-17",
                'descripcion' => "Placa vencida, suspendida o cancelada.",
                'monto' => 1100,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "189-18",
                'descripcion' => "Modificar color consignado en la matrícula.",
                'monto' => 1400,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "189-19",
                'descripcion' => "Tirar o empujar un vehículo por otro vehículo de motor no diseñado para tales fines.",
                'monto' => 1600,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "191",
                'descripcion' => "Emisiones contaminantes y conversion de vehículos de motor.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "192",
                'descripcion' => "Requisitos de equipamientos.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "193",
                'descripcion' => "Obstrucciones a la visibilidad del conductor.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "194",
                'descripcion' => "Aparatos receptores de imágenes.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "195",
                'descripcion' => "Parachoques.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "210",
                'descripcion' => "No porte de licencia de conducir o licencia vencida.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "217",
                'descripcion' => "No porte de seguro o seguro vencido.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "218-219",
                'descripcion' => "Circulación de los peatones.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "220",
                'descripcion' => "Conducción temeraria.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "221",
                'descripcion' => "Distracción durante la conducción.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "222",
                'descripcion' => "Deberes de los conductores hacia los peatones.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "223",
                'descripcion' => "Precaución al encontrarse con un autobús escolar.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "224",
                'descripcion' => "Distancia a mantener entre vehículos.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "225",
                'descripcion' => "Precaución al encontrarse con animales.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "226",
                'descripcion' => "Responsabilidad del propietario de animales.",
                'monto' => 4000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "227-228-229",
                'descripcion' => "Uso de pitos, sirenas, bocinas, luces giratorias, intermitentes o rojas.",
                'monto' => 4000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "230",
                'descripcion' => "Deslizamiento en neutro por cuestas.",
                'monto' => 4000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "231-1",
                'descripcion' => "No uso del cinturón de seguridad.",
                'monto' => 4000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "231-2",
                'descripcion' => "Guía a la derecha.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "231-4",
                'descripcion' => "Lanzar basura o desperdicios a la vía pública.",
                'monto' => 8000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "231-5",
                'descripcion' => "Tomar o dejar pasajeros en zona prohibida y agarrarse de un vehículo de motor en movimiento.",
                'monto' => 6000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "233",
                'descripcion' => "Actividades en vehículos de motor.",
                'monto' => 4000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "235",
                'descripcion' => "Giro prohibido.",
                'monto' => 4000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "236",
                'descripcion' => "Señales de los conductores.",
                'monto' => 2000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "237",
                'descripcion' => "Estacionamientos en lugar prohibido.",
                'monto' => 5000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "238",
                'descripcion' => "Parada en las intersecciones.",
                'monto' => 1400,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "239",
                'descripcion' => "Uso del freno de emergencia.",
                'monto' => 1600,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "241",
                'descripcion' => "Inicio de la marcha.",
                'monto' => 1200,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "246",
                'descripcion' => "Transitar por la derecha.",
                'monto' => 1300,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "247",
                'descripcion' => "Excepciones para el tránsito por la derecha.",
                'monto' => 1500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "248",
                'descripcion' => "Alcanzar y pasar por la izquierda.",
                'monto' => 1900,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "249",
                'descripcion' => "Deber del conductor alcanzado.",
                'monto' => 1700,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],            [
                'numero_articulo' => "250",
                'descripcion' => "Conducción entre carriles.",
                'monto' => 1800,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "251",
                'descripcion' => "No uso del casco protector homologado, chalecos o adimentados de ropas reflectantes.",
                'monto' => 1800,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],            [
                'numero_articulo' => "252",
                'descripcion' => "Cruzarse en sentidos opuestos.",
                'monto' => 1200,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],            [
                'numero_articulo' => "253",
                'descripcion' => "Movimiento en retroceso.",
                'monto' => 1100,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "254",
                'descripcion' => "Ceder el paso.",
                'monto' => 1300,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "256",
                'descripcion' => "Conducir en estado de embriaguez.",
                'monto' => 1500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "257",
                'descripcion' => "Conducción bajo los efectos de drogas o sustancias controladas.",
                'monto' => 1500,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "264",
                'descripcion' => "Límites de velocidad.",
                'monto' => 1700,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "266",
                'descripcion' => "Lugares de velocidad regulada.",
                'monto' => 1800,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "267",
                'descripcion' => "Competencia de velocidad en las vías públicas.",
                'monto' => 2800,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "268",
                'descripcion' => "Límites máximos de velocidad.",
                'monto' => 10000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],
            [
                'numero_articulo' => "269",
                'descripcion' => "Velocidad muy reducida.",
                'monto' => 1000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ],


        ]);
    }
}
