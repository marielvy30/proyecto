<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->boolean('confirmed')->default(0);
            $table->string('confirmation_code')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->string("tipo")->default("1");
            $table->string("cedula")->unique()->nullable();
            $table->date("fecha_nacimiento")->default("2000-01-01");
            $table->string('direccion')->default("");
            $table->string('telefono')->nullable();
            $table->string("nacionalidad")->nullable();
            $table->string("estatus")->nullable();
            $table->string("qr")->unique()->default("");
            $table->boolean('registrado')->default(false);
            $table->string('foto_perfil')->unique()->nullable();
            $table->string('foto_firma')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
