<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("descripcion")->default("");
            $table->string("direccion")->default("");
            $table->string("marca")->default("");
            $table->string("modelo")->default("");
            $table->string("estado")->default("pendiente");
            $table->decimal("monto")->default(0);
            $table->string("placa")->default("");
            $table->string("zona")->default("");
            $table->string("latitud")->default("");
            $table->string("longitud")->default("");
            $table->string("tipo_vehiculo")->default("");
            $table->unsignedBigInteger("policia_id")->nullable(); //policia
            $table->unsignedBigInteger("user_id")->nullable(); //persona
            $table->unsignedBigInteger("pago_id")->nullable(); //persona
            $table->unsignedBigInteger("equipo_id")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multas');
    }
}
