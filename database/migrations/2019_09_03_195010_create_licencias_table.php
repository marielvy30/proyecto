<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("fecha_vencimiento")->default("");
            $table->string("categoria")->default("");
            $table->string("numero_licencia")->default("");
            $table->string("restricciones")->default("");
            $table->string("numeracion")->default("");
            $table->unsignedBigInteger("user_id")->unique(); // persona
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licencias');
    }
}
