<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquiposTable extends Migration
{
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("observacion")->default("");
            $table->string("nombre")->default("");
            $table->string("estado")->nullable();
            $table->string("serial")->nullable();
            $table->unsignedBigInteger("policia_id")->unique()->nullable();
            $table->foreign('policia_id')->references('id')->on('users');
            $table->unsignedBigInteger("impresora_id")->nullable();
            $table->foreign('impresora_id')->references('id')->on('impresoras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipos');
    }
}
