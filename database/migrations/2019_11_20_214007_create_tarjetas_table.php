<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class  CreateTarjetasTable extends Migration
{
    public function up()
    {
        Schema::create('tarjetas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedBigInteger("multa_id"); //multa
            $table->unsignedBigInteger("pago_id"); //articulo
            $table->string("verificacion")->default("");
        });
        Schema::table('tarjetas', function (Blueprint $table) {
            $table->foreign('pago_id')->references('id')->on('pagos');
        });
        Schema::table('tarjetas', function (Blueprint $table) {
            $table->foreign('multa_id')->references('id')->on('multas');
        });
    }
    public function down()
    {
        Schema::dropIfExists('tarjetas');
    }
}
