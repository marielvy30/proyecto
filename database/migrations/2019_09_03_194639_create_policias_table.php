<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
            $table->string("rango")->default("");
            $table->string("tipo_policia")->default("");
            $table->string("tipo")->default("2");
            $table->unsignedBigInteger("user_id")->unique(); //persona
            $table->rememberToken();
            $table->string('password');
            $table->string('email')->unique();
        });

        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
            $table->string("tipo")->default("3");
            $table->unsignedBigInteger("user_id")->unique(); //persona
            $table->rememberToken();
            $table->string('password');
            $table->string('email')->unique();
        });



    }

    public function down()
    {
        Schema::dropIfExists('policias');
        Schema::dropIfExists('admins');
    }
}
