<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPoliciaRelationToMultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('multas', function (Blueprint $table) {
            $table->foreign('policia_id')->references('id')->on('policias');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('multa_articulos', function (Blueprint $table) {
            $table->foreign('multa_id')->references('id')->on('multas');
            $table->foreign('articulo_id')->references('id')->on('articulos');
        });

        Schema::table('policias', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('admins', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('licencias', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('multas', function (Blueprint $table) {
            //
        });
    }
}
