<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    protected $fillable = [
        'name', 'email', 'password','telefono','direccion',
        'tipo','fecha_nacimiento','sexo','cedula','tipo_sangre','nacionalidad',
        'registrado', 'confirmation_code','confirmed','estatus','qr','foto_perfil','foto_firma'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
