<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Pago extends Model
{
    protected $fillable = ["nombre","tarjeta","cvv","mes","ano","balance"];
}
