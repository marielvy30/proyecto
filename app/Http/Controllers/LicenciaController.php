<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class LicenciaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $this->data["count_users"]=User::count();
        return view('registrarLicencia',$this->data);
    }
    public function create()
    {
    }
    public function store(Request $request)
    {
    }
    public function show(Licencia $licencia)
    {
    }
    public function edit(Licencia $licencia)
    {
    }
    public function update(Request $request, Licencia $licencia)
    {
    }
    public function destroy(Licencia $licencia)
    {
    }
}
