<?php

namespace App\Http\Controllers;

use App\Tarjeta;
use Illuminate\Http\Request;

class TarjetaController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show(Tarjeta $tarjeta)
    {
        //
    }


    public function edit(Tarjeta $tarjeta)
    {
        //
    }


    public function update(Request $request, Tarjeta $tarjeta)
    {
        //
    }


    public function destroy(Tarjeta $tarjeta)
    {
        //
    }
}
