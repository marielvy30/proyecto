<?php

namespace App\Http\Controllers;

use App\Antecedente;
use App\Multa;
use App\Policia;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\AssignOp\Mul;

class PoliciaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:policia');
        $this->data["singularModel"] = "Agente";
        $this->data["pluralModel"] = "Agentes";
    }
    public function index()
    {
        return view('homeAgente');
    }

    public function consultarInfraccion(Request $request)
    {
        $this->data["elementos"]=[];

        if ($request->cedula){
            $this->data["usuario"] = User::where('cedula', $request->cedula)->first();
            $this->data["usuarioEncon"] = $this->data["usuario"] !== null;
            if ($this->data["usuarioEncon"]){
                $this->data["elementos"] = Multa::where('user_id',$this->data["usuario"]->id)->with(["user","policia.user","multaarticulo"])->get();
                foreach ($this->data["elementos"] as $elemento) {
                    $elemento->descripcion = "";
                    foreach ($elemento->multaarticulo as $articulo) {
                        $elemento->descripcion .= ($articulo->descripcion . " ");
                    }
                }
            }
        }
        return view('agente.listadoInfraccion', $this->data);
    }

    public function consultarAntecedentes(Request $request)
    {
        $this->data["elementos"]=[];

        if ($request->cedula){
            $this->data["usuario"] = User::where('cedula', $request->cedula)->first();
            $this->data["usuarioEncon"] = $this->data["usuario"] !== null;
            if ($this->data["usuarioEncon"]){
                $this->data["elementos"] = Antecedente::where('user_id',$this->data["usuario"]->id)->get();
            }
        }
        return view('agente.consultarAntecedentes', $this->data);
    }
}
