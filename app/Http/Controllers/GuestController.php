<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class GuestController extends Controller
{
    public function buscarUsuario(Request $request)
    {
        if ($request->cedula)
        {
            $data["usuario"] = User::where('cedula',$request->cedula)->first();
            $data["usuarioEncontrado"] = $data["usuario"] !==null;
            return view('buscarUsuario',$data);
        }
        else
        {
            return view('buscarUsuario');
        }
    }

    public function guardarUsuario(Request $request)
    {
        $data['confirmation_code'] = str_random(25);
        $data['email']=$request->email;
        $data['name']=$request->name;

        $usuario = User::findOrFail($request->id);
        $usuario->update([
            "password"=> $request->password,
            "email"=>$request->email,
            "name"=>$request->name,
            "telefono"=>$request->celular,
            'confirmation_code' => $data['confirmation_code'],
            $request->all()
        ]);

        Mail::send('mails.confirmed_code', $data, function($message) use ($data) {
            $message->to($data['email'], $data['name'])->subject('Por favor confirma tu correo');
        });
        echo "Por favor confirma tu correo";
        return view('auth.login');
    }

    public function verify($code)
    {
        $usuario = User::where('confirmation_code', $code)->first();

        if (! $usuario)
            return redirect('/');

        $usuario->confirmed = true;
        $usuario->confirmation_code = null;
        $usuario->registrado = 1;
        $usuario->password= Hash::make($usuario->password);
       // $usuario->email=>$request->email;
        $usuario->save();

        return redirect('/home')->with('notification', 'Has confirmado correctamente tu correo!');
    }

    public function checkClients()
    {

        $file = File::get(storage_path("clients.txt"));
        $array=[];
        $countNew=0;
        foreach (explode("\n", $file) as $key=>$line){
            if ($line!==""){
                $array[$key] = explode('|', $line);

                $client = User::where('cedula',$array[$key][0])->first();
                if ($client==null){
                    $client = new User();
                    $client->cedula =$array[$key][0];
                    $client->name =$array[$key][1];
                    $client->telefono =$array[$key][2];
                    $client->direccion =$array[$key][3];
                    $client->fecha_nacimiento =$array[$key][4];
                    $client->nacionalidad =$array[$key][5];
                    $client->password = bcrypt('password');
                    $client->qr = $array[$key][6];
                    $client->foto_perfil = $array[$key][0] .'.jpeg';
                    $client->foto_firma = $array[$key][0] .'F.jpeg';
                    $client->save();
                    $countNew++;
                }
            }
        }
        //return ["new"=>$countNew,"users"=>$array];
        return view('administrador.agregarUsuario');
    }

    //Esto es un ejemplo para agregar cliente
    public function addClientToTxt(Request $request)
    {
         $content = "$request->cedula|$request->name|$request->telefono|$request->direccion|$request->fecha_nacimiento|$request->nacionalidad|$request->password|$request->qr|$request->foto_perfil|$request->foto_firma";


        File::append(storage_path("clients.txt"), "\n". $content);
 //       return [$content];
        return view('administrador.agregarUsuario');
    }
}
