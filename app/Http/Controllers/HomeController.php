<?php

namespace App\Http\Controllers;

use App\User;
use App\Multa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        if (Auth::guard("policia")->check()) {
            return view('homeAgente');
        }
        if (Auth::guard("admin")->check()) {
            return view('homeAdministrador');
        }
        if (Auth::guard("web")->check()) {
            return view('homeConductor');
        }
    }
}
