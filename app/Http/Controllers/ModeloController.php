<?php

namespace App\Http\Controllers;

use App\Modelo;
use Illuminate\Http\Request;

class ModeloController extends Controller
{

    public function index()
    {

    }
    public function create()
    {
    }
    public function store(Request $request)
    {
    }
    public function show(Modelo $modelo)
    {
    }
    public function edit(Modelo $modelo)
    {
    }
    public function update(Request $request, Modelo $modelo)
    {
    }
    public function destroy(Modelo $modelo)
    {
    }
}
