<?php

namespace App\Http\Controllers;

use App\Antecedente;
use App\Articulo;
use App\Gps;
use App\Multa;
use App\MultaArticulo;
use App\User;
use App\Marca;
use App\Modelo;
use App\Policia;
use GuzzleHttp\Psr7\ServerRequest;
use Mail;
use Auth;
use Illuminate\Http\Request;
use Nexmo\Laravel\Facade\Nexmo;

class MultaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:policia');
    }
    public function colocacionMultas(Request $request)
    {
        $this->data["coordena"] = Gps::all()->last();
        //dd($this->data["coordena"]);
        $this->data["police"]= Policia::where ('user_id',Auth::user()->id)->first();
        if ($request->cedula)
        {
            $this->data["multas"] = Multa::with(["user","multaarticulo"])->get();
            foreach ($this->data["multas"] as $multa)
            {
                $multa->descripcion = "";
                foreach ($multa->multaarticulo as $articulo)
                {
                    $multa->descripcion .= ($articulo->descripcion. " ");
                }
            }
            $data["usuario"] = User::where('cedula', $request->cedula)->first();

            $this->data["elementos"] = Antecedente::where('user_id',$data["usuario"]->id)->get();

            $data["usuarioEncontra"] = $data["usuario"] !==null;
            return view('/agente/colocacionMultas',$data,$this->data);
        }
        if ($request->cedulaQR)
        {
            $this->data["multas"] = Multa::with(["user","multaarticulo"])->get();
            foreach ($this->data["multas"] as $multa)
            {
                $multa->descripcion = "";
                foreach ($multa->multaarticulo as $articulo)
                {
                    $multa->descripcion .= ($articulo->descripcion. " ");
                }
            }
            $data["usuario"] = User::where('qr', $request->cedulaQR)->first();
            $this->data["elementos"] = Antecedente::where('user_id',$data["usuario"]->id)->get();
            $data["usuarioEncontra"] = $data["usuario"] !==null;
            return view('/agente/colocacionMultas',$data,$this->data);
        }
        else
        {
            return view('/agente/colocacionMultas');
        }
    }



    public function guardarMulta(Request $request)
    {

        if($request->email != null) {

            $data['name'] = $request->name;
            $data['email']  = $request->email;
            $data['direccion']  = $request->direccion;
            Mail:: send('mails.notificacion', $request->all(), function ($msj) use ($data) {
                $msj->subject('Notificación de Multa');
                $msj->to($data['email'], $data['name'], $data['direccion']);
            });
        }
        if($request->mobile != null)
        {
        Nexmo::message()->send([
            'to' => '1' . $request->mobile,
            'from' => '18298922357',
            'text' => 'Hola '. $request->name. ' por el presente medio se le notifica que han violentado un/varios artículo(s) de la ley 63-17 (Leyes de Tránsito)

Agente SIMV: '. Auth::guard('policia')->user()->name.
                '

Lugar del hecho: ' . $request->direccion.
                '

Hora del hecho: ' . $request->hora .
                '

Día del hecho: ' . $request->fecha .
                '
Coordenadas geográficas: latitud, longitud ' .  $request->latitud .','.  $request->longitud .

                '

Para mayor información  acceder a nuestro Sistema de Servicios. http://18.190.117.21/login',

        ]);
        }
        $multa = Multa::create([
            "user_id"=>$request->user_id,
            "tipo_vehiculo"=>$request->tipo_vehiculo,
            "marca"=>$request->marca_id,
            "modelo"=>$request->modelo_id,
            "placa"=>$request->placa,
            "policia_id"=>$request->policia,
            "direccion"=>$request->direccion,
            "latitud" => $request->latitud,
             "longitud" => $request->longitud,
            "descripcion"=>$request->descripcion,
            "monto"=>0,
        ]);
        $monto = 0;
        foreach ($request->articulos as $articulo)
        {
            $articulo = Articulo::where('numero_articulo',$articulo)->first();
            $monto += $articulo->monto;
            MultaArticulo::create([
                "multa_id"=> $multa->id,
                "articulo_id"=>$articulo->id,
            ]);
        }

        $multa->monto = $monto;
        $multa->save();
        $usuario = User::find($request->user_id);
        $this->data["success_msg"] = "Multa aplicada correctamente al usuario: `$usuario->name`" ;
        return view('agente/colocacionMultas',$this->data);
    }

    public function GetModelos(Request $request)
    {
        if ($request->ajax())
        {
            $modelos = Modelo::where('marca_id', $request->marca_id)->get();
            foreach ($modelos as $modelo)
            {
                $modelosArray[$modelo->id] = $modelo->modelo;
            }
            return response()->json($modelosArray);
        }
    }
}

