<?php

namespace App\Http\Controllers;
use App\Articulo;
use App\Impresora;
use App\Equipo;
use App\Multa;
use App\Services\Marcas;
use App\User;
use App\Policia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->data["singularModel"] = "Agente";
        $this->data["pluralModel"] = "Agentes";
    }

    public function localidad(){
        return view('administrador.localidad');
    }
    public function index()
    {
        $this->data["count_multas"] = Multa::count();
        return view('homeAdministrador', $this->data);
    }

    public function listado(Request $request){

        $this->data["elementos"]=[];

        if ($request->id){
            $this->data["usuario"] = Policia::where('id', $request->id)->first();
            $this->data["usuarioEncon"] = $this->data["usuario"] !== null;
        }

        return view('administrador.listado', $this->data);
     }

    public function tablaConsultaAgente()
    {
        $this->data["elementos"] = Policia::all();
        return view('users.index', $this->data);
    }

    public function createAgente(Request $request)
    {
        if ($request->cedula) {
            $data["Agentes"] = User::where('cedula', $request->cedula)->first();
            $data["usuarioEncon"] = $data["Agentes"] !== null;
            return view('administrador/create', $data);
        } else {
            return view('administrador/create');
        }
    }
    public function guardarAgente(Request $request)
    {

        $Agentes = User::findOrFail($request->user_id);
        Policia::create([
            "name" => $request->name,
            "password"=> Hash::make($request->password),
            "user_id"=>$request->user_id,
            "email"=> $request->email,
            $request->all()
        ]);

        $this->data["elementos"] = User::where('tipo','2')->get();
        foreach ($this->data["elementos"] as $policia){
            $policiaModel = Policia::where('user_id',$policia->id)->first();
            $policia->rango = $policiaModel->rango;
            $policia->tipo_policia = $policiaModel->tipo_policia;
        }

        $this->data['agente']= Policia::all();
        $this->data["success_msg"] = "Agente registrado correctamente: `$Agentes->name`" ;
        return view('administrador.create', $this->data);
    }

    public function CrearEquipos()
    {
        $this->data['equipos']= Equipo::all();
        $this->data['prints']= Impresora::all();

        return view('administrador/crearEquipos',$this->data);
    }

    public function guardarEquipo(Request $request)
    {

        if ($request->tipo_equipo == 'impresora') {

            Impresora::create([
                "nombre"=> $request->nombre_i,
                "serial"=> $request->serial,
                "estado"=> $request->estado,
                 $request->all()
            ]);
            $this->data['equipos']= Equipo::all();
            $this->data['prints']= Impresora::all();

            $this->data["success_msg"] = "Equipo registrado correctamente" ;
            return view('administrador/crearEquipos',$this->data);
        }
        elseif ($request->tipo_equipo =='tablet') {

            Equipo::create([
                "nombre"=> $request->nombre_t,
                "serial"=> $request->serial,
                "estado"=> $request->estado,
                $request->all()
            ]);
            $this->data['equipos']= Equipo::all();
            $this->data['prints']= Impresora::all();

            $this->data["success_msg"] = "Equipo registrado correctamente" ;
            return view('administrador/crearEquipos', $this->data);
        }
        else{
            return view('administrador/crearEquipos');
        }
    }

    public function equipos()
    {
        $this->data["elementos"] = Equipo::where('estado','inactivo')->get();
        $this->data["elements"] = Impresora::where('estado','inactivo')->get();

        return view('administrador.consultarEquipos', $this->data);
    }

    public function equiposAlmacen()
    {
        $this->data["elementos"] = Equipo::where('estado','almacen')->get();
        $this->data["elements"] = Impresora::where('estado','inactivo')->get();

        return view('administrador.consultarEquipos', $this->data);
    }
    public function AsignarEquipos(Request $request)
    {

        $this->data["elementos"]=[];

        if ($request->id){
            $this->data["usuario"] = Policia::where('id', $request->id)->first();
            $this->data["usuarioEncon"] = $this->data["usuario"] !== null;
        }

        return view('administrador.AsignarEquipos',$this->data);
    }

    public function AsignacionEquipos(Request $request)
    {
            $equip = Equipo::findOrFail($request->tablet_id);
            $print = Impresora::findOrFail($request->impresora_id);
            $equip->update([
                "estado"=> 'activo',
                "impresora_id"=>$request->impresora_id,
                "policia_id"=>$request->id,
                $request->all()
            ]);
            $print->update([
                "estado"=> 'activo',
                $request->all()
            ]);

        $this->data["success_msg"] = "Equipo registrado correctamente" ;
        return view('administrador.AsignarEquipos',$this->data);
    }
    public function usuarios()
    {
    return view('administrador.agregarUsuario');
    }
    public function mapa()
    {
        return view('administrador.mapa');
    }

    public function consultaEquiposActivos()
    {

        $this->data["equipos"]= Equipo::where('estado', 'activo')->get();
        foreach ($this->data["equipos"] as $equipo)
        {
            $equipo->impresora = Impresora::where("id", $equipo->impresora_id)->first()->nombre;
            $equipo->policia = Policia::where("id", $equipo->policia_id)->first()->name;
        }

        return view('administrador.consultarEquiposAsignados',$this->data);
    }


    public function store(Request $request)
    {
        Policia::create($request->all());
        return redirect()->route('users.index')->with('success', 'Registro creado satisfactoriamente');
    }
    public function edit($id)
    {
        $this->data["elemento"] = Policia::findOrFail($id);;
        return view('users.create', $this->data);
    }

    public function update(Request $request, $id)
    {
        $user = Policia::findOrFail($id);
        $user->update($request->all());
        return redirect()->route('users.index')->with('success', 'Registro actualizado satisfactoriamente');
    }

}
