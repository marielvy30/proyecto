<?php

namespace App\Http\Controllers;

use App\Articulo;
use App\Modelo;
use App\MultaArticulo;
use App\Tarjeta;
use App\User;
use App\Multa;
use App\Marc;
use App\Services\Marcas;
use App\pago;
use Mail;
use Illuminate\Http\Request;
use Auth;
use QrCode;
use Carbon\Carbon;
use Uuid;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->data["singularModel"] = "User";
        $this->data["pluralModel"] = "Users";
    }

    public function create()
    {
        return view('users.create', $this->data);
    }

    public function edit($id)
    {
        $this->data["agente"] = User::findOrFail($id);
        return view('users.create', $this->data);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        return redirect()->route('users.index')->with('success', 'Registro actualizado satisfactoriamente');
    }

    public function store(Request $request)
    {
        $request->password = bcrypt($request->password);
        User::create($request->all());
        return redirect()->route('users.index')->with('success', 'Registro creado satisfactoriamente');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.index')->with('success', 'Registro eliminado satisfactoriamente');
    }

    public function consultaXconductor(Request $request)
    {
        $this->data["elementos"] = [];

        $this->data["usuario"] = User::where('cedula', Auth::user()->cedula)->first();
        $this->data["usuarioEncon"] = $this->data["usuario"] !== null;
        if ($this->data["usuarioEncon"]) {
            $this->data["elementos"] = Multa::where('user_id', Auth::user()->id)->with(["user", "policia.user", "multaarticulo"])->get();
            foreach ($this->data["elementos"] as $elemento) {
                $elemento->modelo = Modelo::where("id", $elemento->modelo)->first()->modelo;
                $elemento->marca = Marcas::where("id", $elemento->marca)->first()->marca;

                $elemento->descripcion = "";
                foreach ($elemento->multaarticulo as $articulo) {
                    $elemento->descripcion .= ($articulo->descripcion . " ");
                }
            }
        }
        return view('conductor.consultaXconductor', $this->data);
    }

    public function consultaConductor()
    {
        $this->data["elementos"] = [];

        $this->data["usuario"] = User::where('cedula', Auth::user()->cedula)->first();
        $this->data["usuarioEncon"] = $this->data["usuario"] !== null;
        if ($this->data["usuarioEncon"]) {
            $this->data["elementos"] = Multa::where('user_id', Auth::user()->id)->with(["user", "policia.user", "multaarticulo"])->get();
            foreach ($this->data["elementos"] as $elemento) {
                $elemento->modelo = Modelo::where("id", $elemento->modelo)->first()->modelo;
                $elemento->marca = Marcas::where("id", $elemento->marca)->first()->marca;
                $elemento->descripcion = "";
                foreach ($elemento->multaarticulo as $articulo) {
                    $elemento->descripcion .= ($articulo->descripcion . " ");
                }
            }
        }
        return view('/agente/consultaConductor', $this->data);
    }




    public function pago(Request $request)
    {
        $this->data["multas"] = [];
        $this->data["pendiente"] = 0;
        $this->data["usuario"] = User::where('cedula', Auth::user()->cedula)->first();
        $this->data["usuarioEncon"] = $this->data["usuario"] !== null;
        if ($this->data["usuarioEncon"]) {
            $this->data["multas"] = Multa::where('user_id', Auth::user()->id)->with(["user", "policia.user", "multaarticulo"])->get();
            foreach ($this->data["multas"] as $multa) {
                if ($multa->estado == "pendiente") {
                    $this->data["pendiente"]++;
                }
                $multa->descripcion = "";
                foreach ($multa->multaarticulo as $articulo) {
                    $multa->descripcion .= ($articulo->descripcion . " ");
                }
            }
        }
        return view('/conductor/pagoMultas', $this->data);
    }

    public function pagos(){

        return view('/homeConductor');

    }

    public function pagando(Request $request)
    {
        $this->data["multas"] = [];
        $this->data["pendiente"] = 0;
        $this->data["usuario"] = User::where('cedula', Auth::user()->cedula)->first();
        $this->data["usuarioEncon"] = $this->data["usuario"] !== null;
        if ($this->data["usuarioEncon"]) {
            $this->data["multas"] = Multa::where('user_id', Auth::user()->id)->with(["user", "policia.user", "multaarticulo"])->get();
            foreach ($this->data["multas"] as $multa) {
                if ($multa->estado == "pendiente") {
                    $this->data["pendiente"]++;
                }
                $multa->descripcion = "";
                foreach ($multa->multaarticulo as $articulo) {
                    $multa->descripcion .= ($articulo->descripcion . " ");
                }
            }
        }




        $this->data["multas"] = Multa::with(["user", "policia.user", "multaarticulo"])->get();
        foreach ($this->data["multas"] as $multa) {
            $multa->descripcion = "";
            foreach ($multa->multaarticulo as $articulo) {
                $multa->descripcion .= ($articulo->descripcion . " ");
            }
        }
        if ($request->tarjeta) {
            $pagos = Pago::where('tarjeta', $request->tarjeta)->first();
            if (!$pagos) {
                $data["success_msg"] = "El número de tarjeta ingresado es incorrecto.";
                return view('conductor.pagoMultas', $data, $this->data);
            }
            if ($request->nombre === $pagos->nombre && $request->cvv === $pagos->cvv && $request->ano === $pagos->ano && $request->mes === $pagos->mes) {
                $verificacion = Uuid::generate()->string;
                $balance = $pagos->balance;
                $monto = 0;
                foreach ($request->seleccionpagos as $seleccionpago) {
                    $seleccionpago = Multa::where('id', $seleccionpago)->with(["user", "policia.user", "multaarticulo"])->first();
                    $monto += $seleccionpago->monto;
                    $facturas[] = $seleccionpago;
                }
                if ($balance >= $monto) {
                    $pagos->balance = $pagos->balance - $monto;

                    $pagos->update([
                        "balance" => $pagos->balance,
                    ]);
                    $pagos->save();
                    foreach ($request->seleccionpagos as $seleccionpago) {
                        $seleccion = Multa::where('id', $seleccionpago)->first();
                        $pagar = Tarjeta::create([
                            "multa_id" => $seleccion->id,
                            "pago_id" => $pagos->id,
                            "verificacion" => $verificacion,
                        ]);
                        $seleccion->update([
                            "estado" => "pagada",
                            "pago_id" => $pagar->id,
                            $request->all()
                        ]);

                        $seleccion->save();
                    }

                    $pdf = \PDF::loadView('ReciboPago', compact('facturas', 'verificacion', 'pagar', 'monto'));

                    Mail:: send('mails.recibopago', $request->all(), function ($msj) use ($pdf) {
                        $msj->subject('Recibo de paga de multa');
                        $msj->to(Auth::user()->email, Auth::user()->name);
                        $msj->attachData($pdf->output(), 'PagoMulta.pdf');
                    });

                    $data["success_msg"] = "Se ha realizado la transacción con éxito.";
                   return view('/homeConductor', $data, $this->data);
                }

                elseif ($balance <= $monto) {

                    $data["success_msg"] = "Balance insuficiente para realizar la transacción.";
                    return view('conductor.pagoMultas', $data, $this->data);
                }

            } elseif ($request->nombre !== $pagos->nombre) {
                $data["success_msg"] = "El nombre ingresado es incorrecto.";
                return view('conductor.pagoMultas', $data, $this->data);
            } elseif ($request->mes !== $pagos->mes) {
                $data["success_msg"] = "El mes ingresado es incorrecto.";
                return view('conductor.pagoMultas', $data, $this->data);
            } elseif ($request->cvv !== $pagos->cvv) {
                $data["success_msg"] = "El CVV ingresado es incorrecto.";
                return view('conductor.pagoMultas', $data, $this->data);
            } elseif ($request->ano !== $pagos->ano) {
                $data["success_msg"] = "El año ingresado es incorrecto.";
                return view('conductor.pagoMultas', $data, $this->data);
            }
        } else {
            $data["success_msg"] = "Hubo un error al realizar la transacción.";
            return view('/conductor/pagoMultas', $data, $this->data);
        }

        $data["success_msg"] = "No tiene saldo suficiente para realizar el pago";
        return view('conductor.pagoMultas', $data, $this->data);

    }


    public function vistaApelar(Request $request)
    {
        $this->data["multas"] = [];

        $this->data["pendiente"] = 0;
        $this->data["pagada"] = 0;

        $this->data["usuario"] = User::where('cedula', Auth::user()->cedula)->first();
        $this->data["usuarioEncon"] = $this->data["usuario"] !== null;
        if ($this->data["usuarioEncon"]) {
            $this->data["multas"] = Multa::where('user_id', Auth::user()->id)->with(["user", "policia.user", "multaarticulo"])->get();

            foreach ($this->data["multas"] as $multa) {
                $hoy = date("d-m-Y");
                $emision = $multa->created_at->format('d-m-Y');
                $fechaEmision = Carbon::parse($emision);
                $fechaActual = Carbon::parse($hoy);

                $this->data["diasDiferencia"] = $fechaActual->diffInDays($fechaEmision);

                if ($multa->estado == "pendiente") {
                    $this->data["pendiente"]++;
                } elseif ($multa->estado == "pagada") {
                    $this->data["pagada"]++;

                }
                $multa->descripcion = "";
                foreach ($multa->multaarticulo as $articulo) {
                    $multa->descripcion .= ($articulo->descripcion . " ");
                }
            }
        }
        return view('/conductor/ImpugnarMulta', $this->data);
    }

    public function apelar(Request $request)
    {
        $this->data["multas"] = Multa::with(["user", "policia.user", "multaarticulo"])->get();
        foreach ($this->data["multas"] as $multa) {
            $multa->descripcion = "";
            foreach ($multa->multaarticulo as $articulo) {
                $multa->descripcion .= ($articulo->descripcion . " ");
            }
        }

        foreach ($request->seleccionApelacion as $seleccionApelacions) {
            $seleccionApelacions = Multa::where('id', $seleccionApelacions)->with(["user", "policia.user", "multaarticulo"])->first();
            $facturas[] = $seleccionApelacions;
            $seleccionApelacions->update([
                "estado" => "apelada",
                $request->all()
            ]);
            $seleccionApelacions->save();
        }

        $solicitud1 = $request->solicitud1;
        $solicitud2 = $request->solicitud2;
        $solicitud3 = $request->solicitud3;
        $solicitud4 = $request->solicitud4;
        $alegacion1 = $request->alegacion1;
        $alegacion2 = $request->alegacion2;
        $alegacion3 = $request->alegacion3;
        $alegacion4 = $request->alegacion4;

        $pdf = \PDF::loadView('mails.formatoApelacion', compact('solicitud1', 'solicitud2', 'facturas', 'solicitud3', 'solicitud4', 'alegacion1', 'alegacion2', 'alegacion3', 'alegacion4'));
        $verificacion = Uuid::generate()->string;

        Mail:: send('mails.Apelacion', $request->all(), function ($msj) use ($pdf) {
            $msj->from(Auth::user()->email);
            $msj->subject('Carta de Apelación de ', (Auth::user()->name), (Auth::user()->cedula));
            $msj->to('tribunaltransitosivm@gmail.com', 'Tribunal Tránsito');
            $msj->attachData($pdf->output(), 'ApelacionMulta.pdf');
        });

        Mail:: send('mails.Apelacion', $request->all(), function ($msj) use ($pdf) {
            $msj->subject('Copia de Carta de Apelación de ', (Auth::user()->name), (Auth::user()->cedula));
            $msj->to(Auth::user()->email, Auth::user()->name);
            $msj->attachData($pdf->output(), 'ApelacionMulta.pdf');
        });


        Mail:: send('mails.NotificacionApelacionRecibida', $request->all(), function ($msj) use ($verificacion) {
            //$msj->from('tribunaltransitosivm@gmail.com');
            $msj->subject('Carta de Apelacion Recibida', (Auth::user()->cedula));
            $msj->to(Auth::user()->email, (Auth::user()->name), $verificacion);
        });

        $data["success_msg"] = "El envio de su apelación se realizó con éxito. Verifique su correo ";
        return view('/homeConductor');
    }
}
