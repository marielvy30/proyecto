<?php

namespace App\Http\Controllers;

use App\Articulo;
use Illuminate\Http\Request;

class ArticuloController extends Controller
{
    public function index()
    {
    }
    public function create()
    {
    }
    public function store(Request $request)
    {
    }
    public function show(Articulo $articulo)
    {
    }
    public function edit(Articulo $articulo)
    {
    }
    public function update(Request $request, Articulo $articulo)
    {
    }
    public function destroy(Articulo $articulo)
    {
    }
}
