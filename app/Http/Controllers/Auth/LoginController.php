<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:policia')->except('logout');
    }

    public function showAdminLoginForm()
    {
        return view('auth.loginAdmin');
    }

    public function showPoliciaLoginForm()
    {
        return view('auth.loginAgente');
    }

    protected function validator(Request $request)
    {
        return $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
    }
    protected function guardLogin(Request $request, $guard)
    {
        $this->validator($request);

        return Auth::guard($guard)->attempt(
            [
                'email' => $request->email,
                'password' => $request->password
            ],
            $request->get('remember')
        );
    }

    public function adminLogin(Request $request)
    {
        if ($this->guardLogin($request, Config::get('constants.guards.admin'))) {
            return redirect()->intended('/homeAdministrador');
        }

        return back()->withInput($request->only('email', 'remember'));
    }

    public function policiaLogin(Request $request)
    {
        if ($this->guardLogin($request,Config::get('constants.guards.policia'))) {
            return redirect()->intended('/homeAgente');
        }

        return back()->withInput($request->only('email', 'remember'));
    }
    public function logout(Request $request)
    {

        if (Auth::guard("policia")->check()) {
            Auth::guard("policia")->logout();
        }
        if (Auth::guard("admin")->check()) {
            Auth::guard("admin")->logout();
        }


        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }
//    public function authenticated($request , $user){
//
//        if(\Auth::user()->tipo==1){
//            return view('homeConductor');
//        }
//        else{
//            return view('auth.login');
//        }
//    }
}
