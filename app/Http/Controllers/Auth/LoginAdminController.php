<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginAdminController extends Controller
{
    use AuthenticatesUsers;
    protected $guard = 'admin';
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    public function authenticated($request , $admin){
        if(\Auth::guard('admin')->user()->tipo==3){
            return view('homeAdministrador');
        }
        else{
            return view('auth.loginAdmin');
        }
    }
    public function showLoginForm()
    {
        return view('auth.loginAdmin');
    }
    public function login(Request $request)
    {
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return view('homeAdministrador');
        }
        return back()->withErrors(['email' => 'Correo o contraseña son incorrecto.']);
    }
    public function logout ()
    {
        Auth::guard('admin')->logout();
        return redirect('/loginAdmin');
    }
}
