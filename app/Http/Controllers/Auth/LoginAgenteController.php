<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Policia;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginAgenteController extends Controller
{
    use AuthenticatesUsers;
    protected $guard = 'policia';
    public function __construct()
    {
        $this->middleware('guest:policia')->except('logout');
    }
    public function authenticated($request , $policia){
         if(\Auth::guard('policia')->user()->tipo==2){
              return view('homeAgente');
          }
        else{
            return view('auth.loginAgente');
        }
    }
    public function showLoginForm()
    {
        return view('auth.loginAgente');
    }
    public function login(Request $request)
    {
        if (Auth::guard('policia')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return view('homeAgente');
        }
        return back()->withErrors(['email' => 'Correo o contraseña son incorrecto.']);
    }
    public function logout ()
    {
        Auth::guard('policia')->logout();
        return redirect('/loginAgente');
    }
}
