<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{

    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard("policia")->check()) {
            return view('homeAgente');
        }
        if (Auth::guard("admin")->check()) {
            return view('homeAdministrador');
        }
        if (Auth::guard($guard)->check()) {
            return view('homeConductor');
        }
        return $next($request);
    }
}
