<?php
namespace App\Services;
use App\Equipo;

use Illuminate\Database\Eloquent\Model;

class Equipos extends Model
{
    public function get()
    {
        $equipos = Equipo::where('estado','inactivo')->get();
        $equiposArray[''] = 'Seleccione Tablet SIMV';
        foreach ($equipos as $equipo) {
            $equiposArray[$equipo->id] = $equipo->nombre;
        }
        return $equiposArray;
    }

}
