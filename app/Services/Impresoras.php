<?php
namespace App\Services;
use App\Impresora;

use Illuminate\Database\Eloquent\Model;

class Impresoras extends Model
{
    public function get()
    {
        $impresoras = Impresora::where('estado','inactivo')->get();
        $impresorasArray[''] = 'Seleccione Lector';
        foreach ($impresoras as $impresora) {
            $impresorasArray[$impresora->id] = $impresora->nombre;
        }
        return $impresorasArray;
    }

}
