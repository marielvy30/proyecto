<?php
namespace App\Services;
use App\Marca;

use Illuminate\Database\Eloquent\Model;

class Marcas extends Model
{
    public function get()
    {
        $marcas = Marca::get();
        $marcasArray[''] = 'Seleccione Marca del Vehículo';
        foreach ($marcas as $marca) {
            $marcasArray[$marca->id] = $marca->marca;
        }
        return $marcasArray;
    }

}
