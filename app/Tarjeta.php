<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarjeta extends Model
{
    protected $fillable = ['multa_id','pago_id','verificacion'];
}
