<?php

namespace App;

use App\Articulo;
use App\Multa;

use Illuminate\Database\Eloquent\Model;

class MultaArticulo extends Model
{

    protected $table = "multa_articulos";
    protected $fillable = ["multa_id","articulo_id"];


    public function articulo(){

        return $this->belongsTo('App\Articulo');
    }
    public function multa(){

        return $this->belongsTo('App\Multa');

    }
}
