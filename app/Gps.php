<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gps extends Model
{
    protected $fillable = ["id","longitude","latitude"];

}
