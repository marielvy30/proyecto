<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Marcas;

class Multa extends Model
{
    protected $table = "multas";
    protected $fillable = ['id','estado', 'tipo_vehiculo','marca','modelo','placa','matricula','latitud','longitud','user_id','policia_id','pago_id',"descripcion","direccion","monto","zona"];

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function policia(){
        return $this->hasOne('App\Policia','id','policia_id');
    }

    public function multaarticulo(){
        return $this->belongsToMany('App\Articulo', 'multa_articulos', 'multa_id', 'articulo_id');
    }
    public function marca(){
        return $this->belongsTo('App\Services\Marcas', 'id', 'marca');
    }

    /*public function marca(){

        return $this->belongsTo('App\Marca');
    }*/

    public function zonaMulta()
    {
        $hour = $this->created_at->format('H');
        if ($hour>11){
            return "STGO-01";
        }else{
            return "STGO-02";
        }

    }
}
