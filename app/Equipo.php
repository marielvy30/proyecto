<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    protected $fillable = ["serial","id","nombre","estado","policia_id","impresora_id","observacion"];


    public function policia(){

        return $this->belongsTo('App\Policia');
    }
    public function impresora(){

        return $this->belongsTo('App\Impresora');

    }
}
