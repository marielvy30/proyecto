<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Policia extends Authenticatable
{
    use Notifiable;
    protected $guard = 'policia';

    protected $fillable = ['rango', 'tipo_policia','user_id','id','email','password','name'];

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }


    protected $hidden = [
        'password', 'remember_token',
    ];

}
