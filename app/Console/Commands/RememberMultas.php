<?php

namespace App\Console\Commands;

use App\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;
use App\Multa;

class RememberMultas extends Command
{
    protected $signature = 'remember:multas';
    protected $description = 'Enviar correo a usuarios con multas pendientes';

    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $this->data["multas"] = Multa::with(["user", "policia.user", "multaarticulo"])->all();
        foreach ($this->data["multas"] as $multa) {
            if ($multa->estado == "pendiente" && $multa->email != null) {
                $multa->email = User::where('id',$multa->user_id)->get()->email;
                Mail::to($multa->email)->send(new SendMailable($multa));
            }
        }
    }
}
