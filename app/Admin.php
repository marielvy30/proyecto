<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';

    protected $fillable = ['user_id','id','email','password','tipo'];

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
    protected $hidden = [
        'password', 'remember_token',
    ];

}
