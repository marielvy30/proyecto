<?php

return [

    'defaults' =>
        [
        'guard' => 'web',
        'passwords' => 'users',
        ],

    'guards' =>
        [
        'web' =>
            [
            'driver' => 'session',
            'provider' => 'users',
            ],
        'api' =>
            [
            'driver' => 'token',
            'provider' => 'users',
            ],
        'policia' =>
            [
            'driver' => 'session',
            'provider' => 'policias',
            ],
        'policia-api' =>
            [
            'driver' => 'token',
            'provider' => 'policias',
            ],
        'admin' =>
            [
            'driver' => 'session',
            'provider' => 'admins',
            ],
        'admin-api' =>
            [
            'driver' => 'token',
            'provider' => 'admins',
            ],
        ],

    'providers' =>
        [
        'users' =>
            [
            'driver' => 'eloquent',
            'model' => App\User::class,
            ],
        'policias' =>
            [
            'driver' => 'eloquent',
            'model' => App\Policia::class,
            ],
        'admins' =>
            [
            'driver' => 'eloquent',
            'model' => App\Admin::class,
            ],
        ],

    'passwords' =>
        [
        'users' =>
            [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 180,
            ],
        'policias' =>
            [
            'provider' => 'policias',
            'table' => 'password_resets',
            'expire' => 180,
            ],
        'admins' =>
            [
            'provider' => 'admins',
            'table' => 'password_resets',
            'expire' => 180,
            ],
        ],
];
